REGION_STR = {
	'r_tarapaca' : 'Región de Tarapacá',
	'r_antofagasta' : 'Región de Antofagasta',
	'r_atacama' : 'Región de Atacama',
	'r_coquimbo' : 'Región de Coquimbo',
	'r_valparaiso' : 'Región de Valparaíso',
	'r_del_libertador_gral__bernardo_o_higgins' : 'Región del Libertador Gral. Bernardo O\'Higgins',
	'r_del_maule' : 'Región del Maule',
	'r_del_biobio' : 'Región del Biobío',
	'r_la_araucania' : 'Región de la Araucanía',
	'r_los_lagos' : 'Región de Los Lagos',
	'r_aisen_del_gral__carlos_ibanez_del_campo' : 'Región Aisén del Gral. Carlos Ibáñez del Campo',
	'r_magallanes_y_de_la_antartica_chilena' : 'Región de Magallanes y de la Antártica Chilena',
	'r_los_rios' : 'Región de Los Ríos',
	'r_arica_y_parinacota' : 'Arica y Parinacota',
	'r_metropolitana' : 'Region Metropolitana',
}

COMMUNE_STR = {
	'c_alto_hospicio' : 'ALTO HOSPICIO',
	'c_camina' : 'CAMINA',
	'c_colchane' : 'COLCHANE',
	'c_huara' : 'HUARA',
	'c_iquique' : 'IQUIQUE',
	'c_pica' : 'PICA',
	'c_pozo_almonte' : 'POZO ALMONTE',
	'c_antofagasta' : 'ANTOFAGASTA',
	'c_calama' : 'CALAMA',
	'c_maria_elena' : 'MARIA ELENA',
	'c_mejillones' : 'MEJILLONES',
	'c_ollague' : 'OLLAGUE',
	'c_san_pedro_de_atacama' : 'SAN PEDRO DE ATACAMA',
	'c_sierra_gorda' : 'SIERRA GORDA',
	'c_taltal' : 'TALTAL',
	'c_tocopilla' : 'TOCOPILLA',
	'c_alto_del_carmen' : 'ALTO DEL CARMEN',
	'c_caldera' : 'CALDERA',
	'c_chanaral' : 'CHANARAL',
	'c_copiapo' : 'COPIAPO',
	'c_diego_de_almagro' : 'DIEGO DE ALMAGRO',
	'c_el_salvador' : 'EL SALVADOR',
	'c_freirina' : 'FREIRINA',
	'c_huasco' : 'HUASCO',
	'c_tierra_amarilla' : 'TIERRA AMARILLA',
	'c_vallenar' : 'VALLENAR',
	'c_a_higuera' : 'A HIGUERA',
	'c_andacollo' : 'ANDACOLLO',
	'c_canela' : 'CANELA',
	'c_combarbala' : 'COMBARBALA',
	'c_coquimbo' : 'COQUIMBO',
	'c_illapel' : 'ILLAPEL',
	'c_la_serena' : 'LA SERENA',
	'c_los_vilos' : 'LOS VILOS',
	'c_monte_patria' : 'MONTE PATRIA',
	'c_ovalle' : 'OVALLE',
	'c_paihuano' : 'PAIHUANO',
	'c_punitaqui' : 'PUNITAQUI',
	'c_rio_hurtado' : 'RIO HURTADO',
	'c_salamanca' : 'SALAMANCA',
	'c_vicuna' : 'VICUNA',
	'c_algarrobo' : 'ALGARROBO',
	'c_cabildo' : 'CABILDO',
	'c_calle_larga' : 'CALLE LARGA',
	'c_cartagena' : 'CARTAGENA',
	'c_casablanca' : 'CASABLANCA',
	'c_catemu' : 'CATEMU',
	'c_concon' : 'CONCON',
	'c_el_quisco' : 'EL QUISCO',
	'c_el_tabo' : 'EL TABO',
	'c_hijuelas' : 'HIJUELAS',
	'c_juan_fernandez' : 'JUAN FERNANDEZ',
	'c_la_calera' : 'LA CALERA',
	'c_la_cruz' : 'LA CRUZ',
	'c_la_ligua' : 'LA LIGUA',
	'c_limache' : 'LIMACHE',
	'c_llay_llay' : 'LLAY LLAY',
	'c_los_andes' : 'LOS ANDES',
	'c_maria_pinto' : 'MARIA PINTO',
	'c_melipilla' : 'MELIPILLA',
	'c_nogales' : 'NOGALES',
	'c_olmue' : 'OLMUE',
	'c_panquehue' : 'PANQUEHUE',
	'c_papudo' : 'PAPUDO',
	'c_petorca' : 'PETORCA',
	'c_puchuncavi' : 'PUCHUNCAVI',
	'c_putaendo' : 'PUTAENDO',
	'c_quillota' : 'QUILLOTA',
	'c_quilpue' : 'QUILPUE',
	'c_quintero' : 'QUINTERO',
	'c_rinconada' : 'RINCONADA',
	'c_san_antonio' : 'SAN ANTONIO',
	'c_san_esteban' : 'SAN ESTEBAN',
	'c_san_felipe' : 'SAN FELIPE',
	'c_san_rafael' : 'SAN RAFAEL',
	'c_santa_maria' : 'SANTA MARIA',
	'c_santo_domingo' : 'SANTO DOMINGO',
	'c_valparaiso' : 'VALPARAISO',
	'c_villa_alemana' : 'VILLA ALEMANA',
	'c_vina_del_mar' : 'VINA DEL MAR',
	'c_zapallar' : 'ZAPALLAR',
	'c_chepica' : 'CHEPICA',
	'c_chimbarongo' : 'CHIMBARONGO',
	'c_codegua' : 'CODEGUA',
	'c_coinco' : 'COINCO',
	'c_coltauco' : 'COLTAUCO',
	'c_donihue' : 'DONIHUE',
	'c_graneros' : 'GRANEROS',
	'c_la_estrella' : 'LA ESTRELLA',
	'c_las_cabras' : 'LAS CABRAS',
	'c_litueche' : 'LITUECHE',
	'c_lolol' : 'LOLOL',
	'c_machali' : 'MACHALI',
	'c_malloa' : 'MALLOA',
	'c_marchigue' : 'MARCHIGUE',
	'c_marchihue' : 'MARCHIHUE',
	'c_mostazal' : 'MOSTAZAL',
	'c_nancagua' : 'NANCAGUA',
	'c_navidad' : 'NAVIDAD',
	'c_olivar' : 'OLIVAR',
	'c_palmilla' : 'PALMILLA',
	'c_paredones' : 'PAREDONES',
	'c_peralillo' : 'PERALILLO',
	'c_peumo' : 'PEUMO',
	'c_pichidegua' : 'PICHIDEGUA',
	'c_pichilemu' : 'PICHILEMU',
	'c_placilla' : 'PLACILLA',
	'c_pumanque' : 'PUMANQUE',
	'c_quinta_de_tilcoco' : 'QUINTA DE TILCOCO',
	'c_rancagua' : 'RANCAGUA',
	'c_rengo' : 'RENGO',
	'c_requinoa' : 'REQUINOA',
	'c_san_fernando' : 'SAN FERNANDO',
	'c_san_francisco_de_mostazal' : 'SAN FRANCISCO DE MOSTAZAL',
	'c_san_vicente' : 'SAN VICENTE',
	'c_santa_cruz' : 'SANTA CRUZ',
	'c_cauquenes' : 'CAUQUENES',
	'c_chanco' : 'CHANCO',
	'c_colbun' : 'COLBUN',
	'c_constitucion' : 'CONSTITUCION',
	'c_curepto' : 'CUREPTO',
	'c_curico' : 'CURICO',
	'c_empedrado' : 'EMPEDRADO',
	'c_hualane' : 'HUALANE',
	'c_licanten' : 'LICANTEN',
	'c_linares' : 'LINARES',
	'c_longavi' : 'LONGAVI',
	'c_maule' : 'MAULE',
	'c_molina' : 'MOLINA',
	'c_parral' : 'PARRAL',
	'c_pelarco' : 'PELARCO',
	'c_pelluhue' : 'PELLUHUE',
	'c_pencahue' : 'PENCAHUE',
	'c_rauco' : 'RAUCO',
	'c_retiro' : 'RETIRO',
	'c_rio_claro' : 'RIO CLARO',
	'c_romeral' : 'ROMERAL',
	'c_sagrada_familia' : 'SAGRADA FAMILIA',
	'c_san_clemente' : 'SAN CLEMENTE',
	'c_san_javier' : 'SAN JAVIER',
	'c_talca' : 'TALCA',
	'c_teno' : 'TENO',
	'c_vichuquen' : 'VICHUQUEN',
	'c_villa_alegre' : 'VILLA ALEGRE',
	'c_yerbas_buenas' : 'YERBAS BUENAS',
	'c_alto_bio_bio' : 'ALTO BIO BIO',
	'c_antuco' : 'ANTUCO',
	'c_arauco' : 'ARAUCO',
	'c_bulnes' : 'BULNES',
	'c_cabrero' : 'CABRERO',
	'c_chiguayante' : 'Chiguayante',
	'c_chillan' : 'CHILLAN',
	'c_cobquecura' : 'COBQUECURA',
	'c_coelemu' : 'COELEMU',
	'c_coihueco' : 'COIHUECO',
	'c_concepcion' : 'CONCEPCION',
	'c_coronel' : 'CORONEL',
	'c_curanilahue' : 'CURANILAHUE',
	'c_el_carmen' : 'EL CARMEN',
	'c_florida' : 'FLORIDA',
	'c_hualqui' : 'HUALQUI',
	'c_laja' : 'LAJA',
	'c_lebu' : 'LEBU',
	'c_los_alamos' : 'LOS ALAMOS',
	'c_los_angeles' : 'LOS ANGELES',
	'c_lota' : 'LOTA',
	'c_mulchen' : 'MULCHEN',
	'c_nacimiento' : 'NACIMIENTO',
	'c_negrete' : 'NEGRETE',
	'c_ninhue' : 'NINHUE',
	'c_niquen' : 'NIQUEN',
	'c_pemuco' : 'PEMUCO',
	'c_penco' : 'PENCO',
	'c_pinto' : 'PINTO',
	'c_portezuelo' : 'PORTEZUELO',
	'c_quilaco' : 'QUILACO',
	'c_quilleco' : 'QUILLECO',
	'c_quillon' : 'QUILLON',
	'c_quirihue' : 'QUIRIHUE',
	'c_ranquil' : 'RANQUIL',
	'c_san_carlos' : 'SAN CARLOS',
	'c_san_fabian' : 'SAN FABIAN',
	'c_san_gregorio_de_niquaon' : 'San Gregorio de NiquÃ©n',
	'c_san_ignacio' : 'SAN IGNACIO',
	'c_san_nicolas' : 'SAN NICOLAS',
	'c_san_patricio' : 'SAN PATRICIO',
	'c_san_pedro_de_la_paz' : 'SAN PEDRO DE LA PAZ',
	'c_san_rosendo' : 'SAN ROSENDO',
	'c_santa_barbara' : 'SANTA BARBARA',
	'c_santa_juana' : 'SANTA JUANA',
	'c_talcahuano' : 'TALCAHUANO',
	'c_tome' : 'TOME',
	'c_treguaco' : 'TREGUACO',
	'c_trehuaco' : 'TREHUACO',
	'c_tucapel' : 'TUCAPEL',
	'c_yumbel' : 'YUMBEL',
	'c_yungay' : 'YUNGAY',
	'c_angol' : 'ANGOL',
	'c_canete' : 'CANETE',
	'c_carahue' : 'CARAHUE',
	'c_collipulli' : 'COLLIPULLI',
	'c_contulmo' : 'CONTULMO',
	'c_cunco' : 'CUNCO',
	'c_curacautin' : 'CURACAUTIN',
	'c_curarrehue' : 'CURARREHUE',
	'c_ercilla' : 'ERCILLA',
	'c_freire' : 'FREIRE',
	'c_galvarino' : 'GALVARINO',
	'c_gorbea' : 'GORBEA',
	'c_lautaro' : 'LAUTARO',
	'c_loncoche' : 'LONCOCHE',
	'c_lonquimay' : 'LONQUIMAY',
	'c_los_sauces' : 'LOS SAUCES',
	'c_lumaco' : 'LUMACO',
	'c_melipeuco' : 'MELIPEUCO',
	'c_nueva_imperial' : 'NUEVA IMPERIAL',
	'c_padre_las_casas' : 'PADRE LAS CASAS',
	'c_panguipulli' : 'PANGUIPULLI',
	'c_perquenco' : 'PERQUENCO',
	'c_pitrufquen' : 'PITRUFQUEN',
	'c_pucon' : 'PUCON',
	'c_puerto_saavedra' : 'PUERTO SAAVEDRA',
	'c_puren' : 'PUREN',
	'c_renaico' : 'RENAICO',
	'c_saavedra' : 'SAAVEDRA',
	'c_temuco' : 'TEMUCO',
	'c_teodoro_schmidt' : 'TEODORO SCHMIDT',
	'c_tirua' : 'TIRUA',
	'c_tolten' : 'TOLTEN',
	'c_traiguen' : 'TRAIGUEN',
	'c_victoria' : 'VICTORIA',
	'c_vilcun' : 'VILCUN',
	'c_villarrica' : 'VILLARRICA',
	'c_ancud' : 'ANCUD',
	'c_calbuco' : 'CALBUCO',
	'c_castro' : 'CASTRO',
	'c_chonchi' : 'CHONCHI',
	'c_cochamo' : 'COCHAMO',
	'c_curacao_de_velez' : 'CURACAO DE VELEZ',
	'c_dalcahue' : 'DALCAHUE',
	'c_fresia' : 'FRESIA',
	'c_frutillar' : 'FRUTILLAR',
	'c_hualaihue' : 'HUALAIHUE',
	'c_llanquihue' : 'LLANQUIHUE',
	'c_los_muermos' : 'LOS MUERMOS',
	'c_maullin' : 'MAULLIN',
	'c_osorno' : 'OSORNO',
	'c_puerto_montt' : 'PUERTO MONTT',
	'c_puerto_octay' : 'PUERTO OCTAY',
	'c_puerto_varas' : 'PUERTO VARAS',
	'c_puqueldon' : 'PUQUELDON',
	'c_purranque' : 'PURRANQUE',
	'c_puyehue' : 'PUYEHUE',
	'c_queilen' : 'QUEILEN',
	'c_quellon' : 'QUELLON',
	'c_quemchi' : 'QUEMCHI',
	'c_quinchao' : 'QUINCHAO',
	'c_rio_negro' : 'RIO NEGRO',
	'c_san_juan_de_la_costa' : 'SAN JUAN DE LA COSTA',
	'c_san_pablo' : 'SAN PABLO',
	'c_aysen' : 'AYSEN',
	'c_chile_chico' : 'CHILE CHICO',
	'c_cisnes' : 'CISNES',
	'c_cochrane' : 'COCHRANE',
	'c_coyhaique' : 'COYHAIQUE',
	'c_guaitecas' : 'GUAITECAS',
	'c_lago_verde' : 'LAGO VERDE',
	'c_ohiggins' : 'OHIGGINS',
	'c_puerto_aisen' : 'PUERTO AISEN',
	'c_puerto_cisnes' : 'PUERTO CISNES',
	'c_rio_ibanez' : 'RIO IBANEZ',
	'c_tortel' : 'TORTEL',
	'c_laguna_blanca' : 'LAGUNA BLANCA',
	'c_navarino' : 'NAVARINO',
	'c_porvenir' : 'PORVENIR',
	'c_primavera' : 'PRIMAVERA',
	'c_puerto_natales' : 'PUERTO NATALES',
	'c_puerto_natales' : 'PUERTO NATALES',
	'c_punta_arenas' : 'PUNTA ARENAS',
	'c_san_gregorio' : 'SAN GREGORIO',
	'c_timaukel' : 'TIMAUKEL',
	'c_torres_del_paine' : 'TORRES DEL PAINE',
	'c_corral' : 'CORRAL',
	'c_futrono' : 'FUTRONO',
	'c_la_union' : 'LA UNION',
	'c_lago_ranco' : 'LAGO RANCO',
	'c_lanco' : 'LANCO',
	'c_los_lagos' : 'LOS LAGOS',
	'c_mafil' : 'MAFIL',
	'c_mariquina' : 'MARIQUINA',
	'c_paillaco' : 'PAILLACO',
	'c_rio_bueno' : 'RIO BUENO',
	'c_valdivia' : 'VALDIVIA',
	'c_arica' : 'ARICA',
	'c_camarones' : 'CAMARONES',
	'c_general_lagos' : 'GENERAL LAGOS',
	'c_putre' : 'PUTRE',
	'c_buin' : 'BUIN',
	'c_calera_de_tango' : 'CALERA DE TANGO',
	'c_cerrillos' : 'CERRILLOS',
	'c_cerro_navia' : 'CERRO NAVIA',
	'c_colina' : 'COLINA',
	'c_conchali' : 'CONCHALI',
	'c_el_bosque' : 'EL BOSQUE',
	'c_el_monte' : 'EL MONTE',
	'c_estacion_central' : 'ESTACION CENTRAL',
	'c_huechuraba' : 'HUECHURABA',
	'c_independencia' : 'INDEPENDENCIA',
	'c_isla_de_maipo' : 'ISLA DE MAIPO',
	'c_la_cisterna' : 'LA CISTERNA',
	'c_la_florida' : 'LA FLORIDA',
	'c_la_granja' : 'LA GRANJA',
	'c_la_pintana' : 'LA PINTANA',
	'c_la_reina' : 'LA REINA',
	'c_lampa' : 'LAMPA',
	'c_las_condes' : 'LAS CONDES',
	'c_lo_barnechea' : 'LO BARNECHEA',
	'c_lo_espejo' : 'LO ESPEJO',
	'c_lo_prado' : 'LO PRADO',
	'c_macul' : 'MACUL',
	'c_maipu' : 'MAIPU',
	'c_nunoa' : 'NUNOA',
	'c_padre_hurtado' : 'PADRE HURTADO',
	'c_paine' : 'PAINE',
	'c_pedro_aguirre_cerda' : 'PEDRO AGUIRRE CERDA',
	'c_penaflor' : 'PENAFLOR',
	'c_penalolen' : 'PENALOLEN',
	'c_pirque' : 'PIRQUE',
	'c_providencia' : 'PROVIDENCIA',
	'c_pudahuel' : 'PUDAHUEL',
	'c_puente_alto' : 'PUENTE ALTO',
	'c_quilicura' : 'QUILICURA',
	'c_quinta_normal' : 'QUINTA NORMAL',
	'c_recoleta' : 'RECOLETA',
	'c_renca' : 'RENCA',
	'c_san_bernardo' : 'SAN BERNARDO',
	'c_san_joaquin' : 'SAN JOAQUIN',
	'c_san_jose_de_maipo' : 'SAN JOSE DE MAIPO',
	'c_san_miguel' : 'SAN MIGUEL',
	'c_san_ramon' : 'SAN RAMON',
	'c_santiago_centro' : 'SANTIAGO CENTRO',
	'c_talagante' : 'TALAGANTE',
	'c_tiltil' : 'TILTIL',
	'c_vitacura' : 'VITACURA',
}

REGION_COMMUNE = {
	'r_tarapaca' : ['c_alto_hospicio', 'c_camina', 'c_colchane', 'c_huara', 'c_iquique', 'c_pica', 'c_pozo_almonte'],
	'r_antofagasta' : ['c_antofagasta', 'c_calama', 'c_maria_elena', 'c_mejillones', 'c_ollague', 'c_san_pedro_de_atacama', 'c_sierra_gorda', 'c_taltal', 'c_tocopilla'],
	'r_atacama' : ['c_alto_del_carmen', 'c_caldera', 'c_chanaral', 'c_copiapo', 'c_diego_de_almagro', 'c_el_salvador', 'c_freirina', 'c_huasco', 'c_tierra_amarilla', 'c_vallenar'],
	'r_coquimbo' : ['c_a_higuera', 'c_andacollo', 'c_canela', 'c_combarbala', 'c_coquimbo', 'c_illapel', 'c_la_serena', 'c_los_vilos', 'c_monte_patria', 'c_ovalle', 'c_paihuano', 'c_punitaqui', 'c_rio_hurtado', 'c_salamanca', 'c_vicuna'],
	'r_valparaiso' : ['c_algarrobo', 'c_cabildo', 'c_calle_larga', 'c_cartagena', 'c_casablanca', 'c_catemu', 'c_concon', 'c_el_quisco', 'c_el_tabo', 'c_hijuelas', 'c_juan_fernandez', 'c_la_calera', 'c_la_cruz', 'c_la_ligua', 'c_limache', 'c_llay_llay', 'c_los_andes', 'c_maria_pinto', 'c_melipilla', 'c_nogales', 'c_olmue', 'c_panquehue', 'c_papudo', 'c_petorca', 'c_puchuncavi', 'c_putaendo', 'c_quillota', 'c_quilpue', 'c_quintero', 'c_rinconada', 'c_san_antonio', 'c_san_esteban', 'c_san_felipe', 'c_san_rafael', 'c_santa_maria', 'c_santo_domingo', 'c_valparaiso', 'c_villa_alemana', 'c_vina_del_mar', 'c_zapallar'],
	'r_del_libertador_gral__bernardo_o_higgins' : ['c_chepica', 'c_chimbarongo', 'c_codegua', 'c_coinco', 'c_coltauco', 'c_donihue', 'c_graneros', 'c_la_estrella', 'c_las_cabras', 'c_litueche', 'c_lolol', 'c_machali', 'c_malloa', 'c_marchigue', 'c_marchihue', 'c_mostazal', 'c_nancagua', 'c_navidad', 'c_olivar', 'c_palmilla', 'c_paredones', 'c_peralillo', 'c_peumo', 'c_pichidegua', 'c_pichilemu', 'c_placilla', 'c_pumanque', 'c_quinta_de_tilcoco', 'c_rancagua', 'c_rengo', 'c_requinoa', 'c_san_fernando', 'c_san_francisco_de_mostazal', 'c_san_vicente', 'c_santa_cruz'],
	'r_del_maule' : ['c_cauquenes', 'c_chanco', 'c_colbun', 'c_constitucion', 'c_curepto', 'c_curico', 'c_empedrado', 'c_hualane', 'c_licanten', 'c_linares', 'c_longavi', 'c_maule', 'c_molina', 'c_parral', 'c_pelarco', 'c_pelluhue', 'c_pencahue', 'c_rauco', 'c_retiro', 'c_rio_claro', 'c_romeral', 'c_sagrada_familia', 'c_san_clemente', 'c_san_javier', 'c_talca', 'c_teno', 'c_vichuquen', 'c_villa_alegre', 'c_yerbas_buenas'],
	'r_del_biobio' : ['c_alto_bio_bio', 'c_antuco', 'c_arauco', 'c_bulnes', 'c_cabrero', 'c_chiguayante', 'c_chillan', 'c_cobquecura', 'c_coelemu', 'c_coihueco', 'c_concepcion', 'c_coronel', 'c_curanilahue', 'c_el_carmen', 'c_florida', 'c_hualqui', 'c_laja', 'c_lebu', 'c_los_alamos', 'c_los_angeles', 'c_lota', 'c_mulchen', 'c_nacimiento', 'c_negrete', 'c_ninhue', 'c_niquen', 'c_pemuco', 'c_penco', 'c_pinto', 'c_portezuelo', 'c_quilaco', 'c_quilleco', 'c_quillon', 'c_quirihue', 'c_ranquil', 'c_san_carlos', 'c_san_fabian', 'c_san_gregorio_de_niqua(c)n', 'c_san_ignacio', 'c_san_nicolas', 'c_san_patricio', 'c_san_pedro_de_la_paz', 'c_san_rosendo', 'c_santa_barbara', 'c_santa_juana', 'c_talcahuano', 'c_tome', 'c_treguaco', 'c_trehuaco', 'c_tucapel', 'c_yumbel', 'c_yungay'],
	'r_la_araucania' : ['c_angol', 'c_canete', 'c_carahue', 'c_collipulli', 'c_contulmo', 'c_cunco', 'c_curacautin', 'c_curarrehue', 'c_ercilla', 'c_freire', 'c_galvarino', 'c_gorbea', 'c_lautaro', 'c_loncoche', 'c_lonquimay', 'c_los_sauces', 'c_lumaco', 'c_melipeuco', 'c_nueva_imperial', 'c_padre_las_casas', 'c_panguipulli', 'c_perquenco', 'c_pitrufquen', 'c_pucon', 'c_puerto_saavedra', 'c_puren', 'c_renaico', 'c_saavedra', 'c_temuco', 'c_teodoro_schmidt', 'c_tirua', 'c_tolten', 'c_traiguen', 'c_victoria', 'c_vilcun', 'c_villarrica'],
	'r_los_lagos' : ['c_ancud', 'c_calbuco', 'c_castro', 'c_chonchi', 'c_cochamo', 'c_curacao_de_velez', 'c_dalcahue', 'c_fresia', 'c_frutillar', 'c_hualaihue', 'c_llanquihue', 'c_los_muermos', 'c_maullin', 'c_osorno', 'c_puerto_montt', 'c_puerto_octay', 'c_puerto_varas', 'c_puqueldon', 'c_purranque', 'c_puyehue', 'c_queilen', 'c_quellon', 'c_quemchi', 'c_quinchao', 'c_rio_negro', 'c_san_juan_de_la_costa', 'c_san_pablo'],
	'r_aisen_del_gral__carlos_ibanez_del_campo' : ['c_aysen', 'c_chile_chico', 'c_cisnes', 'c_cochrane', 'c_coyhaique', 'c_guaitecas', 'c_lago_verde', 'c_ohiggins', 'c_puerto_aisen', 'c_puerto_cisnes', 'c_rio_ibanez', 'c_tortel'],
	'r_magallanes_y_de_la_antartica_chilena' : ['c_laguna_blanca', 'c_navarino', 'c_porvenir', 'c_primavera', 'c_puerto_natales', 'c_puerto_natales', 'c_punta_arenas', 'c_san_gregorio', 'c_timaukel', 'c_torres_del_paine'],
	'r_los_rios' : ['c_corral', 'c_futrono', 'c_la_union', 'c_lago_ranco', 'c_lanco', 'c_los_lagos', 'c_mafil', 'c_mariquina', 'c_paillaco', 'c_rio_bueno', 'c_valdivia'],
	'r_arica_y_parinacota' : ['c_arica', 'c_camarones', 'c_general_lagos', 'c_putre'],
	'r_metropolitana' : ['c_buin', 'c_calera_de_tango', 'c_cerrillos', 'c_cerro_navia', 'c_colina', 'c_conchali', 'c_el_bosque', 'c_el_monte', 'c_estacion_central', 'c_huechuraba', 'c_independencia', 'c_isla_de_maipo', 'c_la_cisterna', 'c_la_florida', 'c_la_granja', 'c_la_pintana', 'c_la_reina', 'c_lampa', 'c_las_condes', 'c_lo_barnechea', 'c_lo_espejo', 'c_lo_prado', 'c_macul', 'c_maipu', 'c_nunoa', 'c_padre_hurtado', 'c_paine', 'c_pedro_aguirre_cerda', 'c_penaflor', 'c_penalolen', 'c_pirque', 'c_providencia', 'c_pudahuel', 'c_puente_alto', 'c_quilicura', 'c_quinta_normal', 'c_recoleta', 'c_renca', 'c_san_bernardo', 'c_san_joaquin', 'c_san_jose_de_maipo', 'c_san_miguel', 'c_san_ramon', 'c_santiago_centro', 'c_talagante', 'c_tiltil', 'c_vitacura'],
}



var helpers = {
	closeCardModal: function(id){
		$('#'+id).modal('hide');
	},

	printFrame: function(id){
		frm = document.getElementById(id).contentWindow;
		frm.focus();
		frm.print();
	},

	parseEsFloat: function(esFloatString) {
		const isEn = esFloatString[esFloatString.length - 2] == '.';

		if (isEn) {
			var enString = esFloatString;
		} else {
			var enString = esFloatString.replace(/\./g, '').replace(',', '.');
		}

		return parseFloat(enString);
	},

	setReadjustmentDateHandlers: function(){
		var readjustmentFromFields = $('.from-date-field');
		var readjustmentUntilFields = $('.until-date-field');
		var readjustmentTotalFields = $('.total-rent');
		var readjustmentAmountFields = $('[id^=contract_readjustments_attributes_][id$=_amount]');
		var n_fields = readjustmentFromFields.length;
		var startDateField = document.getElementById('start-date');
		var expirationDateField = document.getElementById('expiration-date');
		var rentField = document.getElementById('contract_rent');

		// Date adjusters
		// Set initial values
		readjustmentFromFields[0].value = startDateField.value;
		readjustmentUntilFields[n_fields-1].readOnly = true;
		readjustmentUntilFields[n_fields-1].value = expirationDateField.value;
		for(let i = 1; i < n_fields; i++){
			readjustmentFromFields[i].value = readjustmentUntilFields[i-1].value
		}

		// First from listener
		startDateField.onchange = function(evt){
			readjustmentFromFields[0].value = evt.target.value;
		}

		// Middle untilslsiteners
		for(let i = 1; i < n_fields; i++){
			readjustmentUntilFields[i-1].onchange = function(evt){
				readjustmentFromFields[i].value = evt.target.value
			}
		}

		// Last until listener
		expirationDateField.onchange = function(evt){
			readjustmentUntilFields[n_fields - 1].value = evt.target.value;
		}

		// Total adjusters
		// Set initial values
		for(let i = 0; i < n_fields; i++){
			var amounts = extractValuesFrom(readjustmentAmountFields, i);
			var sumAmounts = sum(amounts);
			var readjustmentTotal = helpers.parseEsFloat(rentField.value) + sumAmounts;
			readjustmentTotalFields[i].value = readjustmentTotal;
		}

		// Rent listener
		rentField.onchange = function(evt){
			readjustmentTotalFields[0].value = helpers.formatFloat((helpers.parseEsFloat(evt.target.value) + helpers.parseEsFloat(readjustmentAmountFields[0].value)).toString(), fromSrv = true);
			for(let i = 1; i < n_fields; i++){
				var amounts = extractValuesFrom(readjustmentAmountFields, i)
				var sumAmounts = sum(amounts);
				var readjustmentTotal = helpers.parseEsFloat(evt.target.value) + sumAmounts;
				readjustmentTotalFields[i].value = helpers.formatFloat(readjustmentTotal.toString(), fromSrv = true);
			}
		}

		// Readjustment amount listeners
		for(let i = 0; i < n_fields; i++){
			readjustmentAmountFields[i].onchange = function(){
				for(let j = 0; j < n_fields; j++){
					var amounts = extractValuesFrom(readjustmentAmountFields, j);
					var sumAmounts = sum(amounts);
					var readjustmentTotal = helpers.parseEsFloat(rentField.value) + sumAmounts;
					readjustmentTotalFields[j].value = helpers.formatFloat(readjustmentTotal.toString(), fromSrv = true);
				}
			}
		}

	},

	triggerUploadButton: function(id){
		document.getElementById(id).click();
	},

	setRutFieldListener: function(field_id){
		$('#'+field_id).off('keydown');
		$('#'+field_id).on('keydown', function(evt){
			if(!keyCodeIsValid(evt.keyCode)){
				return;
			}else if (evt.target.value.length == 10 || evt.target.value.length == 11) {
				null;
			}else if(!keyCodeIsNumber(evt.keyCode)){
				evt.preventDefault();
				return;
			}

			let oldVal = $(this).val() + evt.key;

			// remove unformatted dots and slashes
			const valWithNoDotOrCommas = oldVal.replace(/\.|-/g, '');

			// correctly place dot and slashes
			const valWithDotNCommasPrev = valWithNoDotOrCommas.replace(
			  /^(\d{0,2})(\d{0,3})(\d{0,3})(\w{0,1})(\d*)$/g,
			  '$1.$2.$3-$4'
			);

			// remove trailing dot and slashes
			const valWithDotNCommasFinal = valWithDotNCommasPrev.replace(/(\.|-)*$/, '');

			$(this).val(valWithDotNCommasFinal);

			evt.preventDefault();
		});
	},

	setRutFieldListenerByClass: function(field_class){
		$('.'+field_class).off('keydown');
		$('.'+field_class).on('keydown', function(evt){
			if(!keyCodeIsValid(evt.keyCode)){
				return;
			}else if (evt.target.value.length == 10 || evt.target.value.length == 11) {
				null;
			}else if(!keyCodeIsNumber(evt.keyCode)){
				evt.preventDefault();
				return;
			}

			let oldVal = $(this).val() + evt.key;

			// remove unformatted dots and hyphens
			const valWithNoDotOrCommas = oldVal.replace(/\.|-/g, '');

			// correctly place dot and hyphens
			const valWithDotNCommasPrev = valWithNoDotOrCommas.replace(
			  /^(\d{0,2})(\d{0,3})(\d{0,3})(\w{0,1})(\d*)$/g,
			  '$1.$2.$3-$4'
			);

			// remove trailing dot and hyphens
			const valWithDotNCommasFinal = valWithDotNCommasPrev.replace(/(\.|-)*$/, '');

			$(this).val(valWithDotNCommasFinal);

			evt.preventDefault();
		});
	},

	formatFloat: function(floatString, fromSrv = false) {
		const enString =  fromSrv ? floatString : floatString.replace(/\./g, '').replace(',', '.');
		const suf = enString[enString.length -1] == '.' ? ',' : ''
		var rawVal = parseFloat(enString);
		const isFloat = rawVal % 1 != 0;
		if (!isNaN(rawVal)) {
			rawVal = isFloat ? parseFloat(rawVal.toFixed(2)) : rawVal
			return rawVal.toLocaleString(locales = 'es') + suf;
		} else {
			return '';
		}
	},

	setFloatFieldListenerByClass: function(field_class, fromSrv = true) {
		// Format initial value
		const fields = document.getElementsByClassName(field_class)

		for (var i = 0; i < fields.length; i++ ){
			console.log(fields[i].value);
			fields[i].value = helpers.formatFloat(fields[i].value, fromSrv = fromSrv);
			console.log(fields[i].value);
		}

		// Avoid any keys that are numbers or backspace
		$('.' + field_class).off('keydown');
		$('.' + field_class).on('keydown', function(evt){
			if (!keyCodeIsNumber(evt.keyCode)) {
				evt.preventDefault();
				return;
			}
		});

		// Format into currency
		$('.' + field_class).off('keyup');
		$('.' + field_class).on('keyup', function(evt){
			evt.target.value = helpers.formatFloat(evt.target.value);
		});
	},

	showModalWait: function(){
		modal.configDelete();
		modal.reset();
		modal.setBody('Procesando...');
		modal.show();
	},

	updateCommuneByRegion: function(region, communeSelectId){
		var communeSelect = document.getElementById(communeSelectId);
		var selectedCommune = communeSelect.options[communeSelect.selectedIndex].value;
		var commune_values = REGION_COMMUNE[region];
		var commune_strings = commune_values.map(function(val){return COMMUNE_STR[val]});

		while(communeSelect.firstChild){
			communeSelect.removeChild(communeSelect.firstChild);
		}

		for(var i = 0; i < commune_values.length; i++){
			var opt = document.createElement('option');
			opt.value = commune_values[i];
			opt.innerHTML = commune_strings[i];
			if (opt.value == selectedCommune){
				opt.selected = true;
			}
			communeSelect.appendChild(opt)
		}
	},

	exportTableToExcel: function(tableId){
		var downloadLink;
		var dataType = 'application/vnd.ms-excel;charset=utf-8';
		var table = document.getElementById(tableId).cloneNode(true);
		var tableRows = table.rows;
		var tableHeader = tableRows[0];
		var tableHTML;

		// Filter rows to avoid putting links in the xls
		for(var i = 0; i < tableHeader.cells.length; i++){
			cellContent = tableHeader.cells[i].innerHTML.trim().toLowerCase()
			if(['acción', 'accion', 'acciones'].includes(cellContent)){
				for (var j = 0; j < tableRows.length; j++) {
					tableRows[j].deleteCell(i);
				}
			}
		}

		// Tamper with the table html
		tableHTML = table.outerHTML.replace(/ /g, '%20');

		// Specify file name
		filename = 'excel_data.xls';

		// Create download link element
		downloadLink = document.createElement("a");
		document.body.appendChild(downloadLink);

		if(navigator.msSaveOrOpenBlob){
			var blob = new Blob(['\ufeff', tableHTML], {
				type: dataType
			});
			navigator.msSaveOrOpenBlob( blob, filename);
		}else{
			console.log('im going here');
			// Create a link to the file
			downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
			console.log(tableHTML);

			// Setting the file name
			downloadLink.download = filename;

			//triggering the function
			downloadLink.click();
		}
	},

	downloadCsvFromTable: function(tableId, filename = 'tabla'){

		filename += '.csv';
		var table = document.getElementById(tableId);
		var tableHeader = table.rows[0];
		var forbiddenRows = ['acción', 'accion', 'acciones'];

		var csvContent = '';
		for(var j = 0; j < table.rows.length; j++){
			for(var i = 0; i < tableHeader.cells.length; i++){
				headerCellContent = tableHeader.cells[i].innerHTML.trim().toLowerCase();
				if(!forbiddenRows.includes(headerCellContent)){
					csvContent += table.rows[j].cells[i].innerHTML + ','
				}
			}
			csvContent = csvContent.substring(0, csvContent.length -1) + '\n';
		}

		var encodedUri = encodeURI(csvContent);
		var downloadLink = document.createElement('a');
		downloadLink.href = 'data:text/csvlcharset=utf-8,\uFEFF' + encodedUri;
		downloadLink.download = filename;
		downloadLink.click();
	},

	downloadXlsxFromTable: function(tableId, filename = 'tabla') {
		//filename += '.xlsx';
		var table = document.getElementById(tableId);
		var tableHeader = table.rows[0];
		var forbiddenRows = ['acción', 'accion', 'acciones'];

		// Need to build data matrix to create xlsx
		var data = [];
		for(var j = 0; j < table.rows.length; j++){
			data.push([]);
			for(var i = 0; i < tableHeader.cells.length; i++){
				headerCellContent = tableHeader.cells[i].innerHTML.trim().toLowerCase();
				if(!forbiddenRows.includes(headerCellContent)){
					data[j].push({
						value : table.rows[j].cells[i].innerHTML,
						type: 'string'
					});
				}
			}
		}

		var config = {
			filename: filename,
			sheet: {
				data : data
			}
		}
		zipcelx(config);
	},

	downloadXlsxFromFakeTable: function(fakeTableId, filename = 'tabla') {
		filename += '.xlsx';
		var fakeTable = document.getElementById(fakeTableId);
		var fakeTableHeader = fakeTable.getElementsByClassName('faketable-header')[0];
		var fakeTableRows = fakeTable.getElementsByClassName('faketable-row');
		var forbiddenRows = ['acción', 'accion', 'acciones'];

		var data = [];
		for (var j = 0; j < fakeTableRows.length; j++) {
			data.push([]);

			for (var i = 0; i < fakeTableHeader.children.length; i++) {
				var headerCellContent = fakeTableHeader.children[i].innerHTML.trim().toLowerCase();

				if (!forbiddenRows.includes(headerCellContent)) {
					var value = extractFakeCellValue(fakeTableRows[j].children[i])

					data[j].push({
						value : value,
						type: 'string'
					});
				}
			}
		}

		var config = {
			filename: filename,
			sheet: {
				data: data
			}
		}
		zipcelx(config);
	},

	setFilename: function(evt){
		var fileNameField = evt.target.parentNode.getElementsByClassName('file-name')[0];
		var fileName = evt.target.value.split('\\').slice(-1).pop();
		fileNameField.innerHTML = fileName;
	},

	fileUploadSuccess: function(evt){
		var labelField = evt.target.parentNode.getElementsByClassName('btn-info')[0];
		labelField.classList.remove('btn-info');
		labelField.classList.add('btn-success');
	}
}

function extractValuesFrom(numberInputs, i){
	values = numberInputs.map(function(index, input){
		if(index <= i){
			//return parseFloat(input.value);
			return helpers.parseEsFloat(input.value);
		}
		return 0;
	});

	return values;
}

function sum(numberArray){
	total = 0;
	for(let j = 0; j < numberArray.length; j++){
		total += numberArray[j];
	}

	return total;
}

function keyCodeIsValid(keycode){
	var valid =
        (keycode > 47 && keycode < 58)   || // number keys
        keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
        (keycode > 64 && keycode < 91)   || // letter keys
        (keycode > 95 && keycode < 112)  || // numpad keys
        (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
        (keycode > 218 && keycode < 223);   // [\]' (in order)

    return valid
}

function keyCodeIsNumber(keycode){
	var valid = (keycode > 47 && keycode < 58) ||
				(keycode > 95 && keycode < 112) ||
				keycode == 188 || keycode == 190 ||keycode == 8 ||
				keycode == 9 || keycode == 16 || keycode == 36 ||
				keycode == 35 || keycode == 17 || keycode == 18;

	return valid;
}

function extractFakeCellValue(fakeCell) {

	var child = fakeCell;
	while(child.children.length == 1){
		child = child.children[0];
	}

	if (child.nodeName == 'DIV' && child.children.length == 0){

		return child.innerHTML;

	} else if (child.nodeName == 'INPUT' && child.classList.contains('date_picker')){

		return child.value;

	} else if (child.nodeName == "SELECT") {
		for (var k = 0; k < child.children.length; k++) {
			if (child.children[k].selected){

				return child.children[k].innerHTML;
			}
		}

	} else {
		console.log('unkown fake cell');
		console.log(child);

		return 'unknown';
	}
}
