// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

/* Administration number AJAX */
/*
function setCreateNumberAjax(){
	$('#create-administration-number').on('click', function(evt){
		var old_admin_n = document.getElementById('old-admin-n').value;
		var user_id =  document.getElementById('user-id').value;;
		$.ajax({
			type: "POST",
			url: '/properties/create_administration_number',
			data: {
				'old_admin_n': old_admin_n,
				'user_id': user_id,
			},
			success: function(response){
				eval(response);
				setCreateNumberAjax();
			}
		});
		evt.preventDefault();
	});
}
setCreateNumberAjax();

function setAssignNumberAjax(){
	$('#assign-administration-number').on('click', function(evt){
		var admin_n = document.getElementById('administration-number-select').value;
		var old_admin_n = document.getElementById('old-admin-n').value;
		var user_id =  document.getElementById('user-id').value;;
		$.ajax({
			type: "POST",
			url: '/properties/assign_administration_number',
			data: {
				'admin_n': admin_n,
				'old_admin_n': old_admin_n,
				'user_id': user_id,
			},
			success: function(response){
				eval(response);
				setAssignNumberAjax();
			}
		});
		evt.preventDefault();
	});
}
setAssignNumberAjax();*/

/* Common Expenses Administrator handling */
function setCommonExpensesDisable(){
	function toggleDisabled(value, container){
		if(value == 'false'){
			container.attr('disabled', true);
		}else{
			container.attr('disabled', false);
		}
	}

	commonExpenses = document.getElementById('common-expenses');
	commonExpensesContainer = $('#common-expenses-container :input');

	//console.log(commonExpenses.value);
	toggleDisabled(commonExpenses.value, commonExpensesContainer);
	commonExpenses.onchange = function(evt){
		toggleDisabled(evt.target.value, commonExpensesContainer);
	}
}

