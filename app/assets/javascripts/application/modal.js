var modal = {

	modalObject: null,

	modalHeader: null,

	modalBody: null,

	modalProcessingBody: null,

	modalFooter: null,

	closeable: true,

	headerH4Tag: function(text){
		return '<h4 class="modal-title d-inline text-uppercase" id="general-modal-header">'+text+'</h4>';
	},

	headerH5Tag: function(text){
		return '<h5 class="modal-title" id="general-modal-header">'+text+'</h5>';
	},

	closeButton: function(){
		return '<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>';
	},

	closeCross: function(){
		return '<button type="button" class="close" id="close-cross" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	},

	insertModalObject: function(){
		modalHtml = `
			<div class="modal fade" tabindex="-1" id="general-modal" role="dialog" >
				<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
					<div class="modal-content" id="general-modal-content">

						<div id="modal-header" class="modal-header d-block text-center bg-light p-3">
							${modal.headerH4Tag('')}
							${modal.closeCross()}
						</div>

						<div class="modal-body" id="general-modal-body"></div>

						<div id="processing-modal-body"></div>

						<div class="modal-footer bg-whitesmoke br" id="general-modal-footer"></div>

					</div>
				</div>
			</div>`
		mainWrapper = $('#app');
		mainWrapper.append(modalHtml);
	},

	fetchModalObject: function(){
		if( modal.modalObject == null){
			modal.modalObject = $('#general-modal');

			if( modal.modalObject.length < 1){
				modal.insertModalObject();
				modal.modalObject = $('#general-modal');
			}
		}

		return modal.modalObject;
	},

	fetchModalHeader: function(){
		if( modal.modalHeader == null){
			modalObject = modal.fetchModalObject();
			modal.modalHeader = modalObject.find('#general-modal-header');
		}

		return modal.modalHeader;
	},

	fetchModalBody: function(){
		if( modal.modalBody == null){
			modalObject = modal.fetchModalObject();
			modal.modalBody = modalObject.find('#general-modal-body');
		}

		return modal.modalBody;
	},

	fetchModalProcessingBody: function(){
		if( modal.modalProcessingBody == null){
			modalObject = modal.fetchModalObject();
			modal.modalProcessingBody = modalObject.find('#processing-modal-body');
		}

		return modal.modalProcessingBody;
	},

	fetchModalFooter: function(){
		if( modal.modalFooter == null){
			modalObject = modal.fetchModalObject();
			modal.modalFooter = modalObject.find('#general-modal-footer');
		}

		return modal.modalFooter;
	},

	show: function(){
		modalObject = modal.fetchModalObject();
		modalObject.modal('show');
	},

	hide: function(){
		modalObject = modal.fetchModalObject();
		modalObject.modal('hide');
	},

	setHeader: function(html){
		modalHeader = modal.fetchModalHeader();
		modalHeader.html(html);
	},

	setBody: function(html){
		modalContent = modal.fetchModalBody();
		modalContent.html(html);
	},

	setProcessingBody: function(html){
		modalContent = modal.fetchModalProcessingBody();
		modalContent.html(html);
	},

	setFooter: function(html){
		modalFooter = modal.fetchModalFooter();
		modalFooter.html(html);
	},

	hideBody: function(){
		modalContent = modal.fetchModalBody()
		modalContent.attr('hidden', true);
	},

	unhideBody: function(){
		modalContent = modal.fetchModalBody()
		modalContent.attr('hidden', false);
	},

	reset: function(){
		modal.setHeader('');
		modal.setBody('');
		modal.setProcessingBody('');
		modal.setFooter('');
	},

	makeUncloseable: function(){
		if(modal.closeable){
			modalObject = modal.fetchModalObject();

			// Remove cross if present
			cross = modalObject.find('#close-cross');
			if(cross.length > 0){
				modalObject.find('#close-cross').remove();
			}

			/*
			// Configure modal
			modalData = modalObject.data('bs.modal') || {_isShown: false};
			initialized = modalData._isShown;

			if(initialized) modal.hide();
			modalObject.removeData("bs.modal");
			modalObject.modal({
				backdrop: 'static',
				keyboard: false,

			});
			if(initialized) modal.show();*/
			modal.closeable = false;
		}
	},

	makeCloseable: function(){
		if(!(modal.closeable)){
			modalObject = modal.fetchModalObject();

			// Add cross if not present
			cross = modalObject.find('#close-cross');
			if(cross.length == 0){
				modalObject.find('#modal-header').append(modal.closeCross())
			}

			/*
			// Configure modal
			modalData = modalObject.data('bs.modal') || {_isShown: false};
			initialized = modalData._isShown;

			if(initialized) modal.hide();
			modalObject.removeData("bs.modal");
			modalObject.modal({
				backdrop: true,
				keyboard: true
			});
			if(initialized) modal.show();*/
			modal.closeable = true;
		}
	},

	configXl: function(){
		// Configure modal content to by XL
		modalObject = modal.fetchModalObject();
		modalDialog = modalObject.children();
		modalDialog.addClass('modal-xl');
		modalDialog[0].style.minWidth = "95%";

		// Change modal-header class
		modalHeaderDiv = modalObject.find('.modal-header')
		modalHeaderDiv.removeClass().addClass('modal-header d-block text-center bg-light p-3');

		// Change title to h4
		modalTitle = modalObject.find('.modal-title')
		modalTitle.replaceWith($(modal.headerH4Tag(modalTitle.html())));
		modal.modalHeader = null;

		modal.unhideBody();
		modal.makeCloseable();
	},

	configDelete: function(){
		// Configure modal content to be normal
		modalObject = modal.fetchModalObject();
		modalDialog = modalObject.children();
		modalDialog.removeClass('modal-xl');
		modalDialog[0].style.minWidth = "";

		// Change modal-header class
		modalHeaderDiv = modalObject.find('.modal-header')
		modalHeaderDiv.removeClass().addClass('modal-header');

		// Change title to h5
		modalTitle = modalObject.find('.modal-title')
		modalTitle.replaceWith($(modal.headerH5Tag(modalTitle.html())));
		modal.modalHeader = null;

		modal.unhideBody();
		modal.makeCloseable();
	},

	configProcessing: function(){
		modal.makeUncloseable();

		// Configure modal content to be normal
		modalObject = modal.fetchModalObject();
		modalDialog = modalObject.children();
		modalDialog.removeClass('modal-xl');
		modalDialog.addClass('modal-sm');
		modalDialog[0].style.minWidth = "";

		modal.hideBody();
		modal.setHeader('');
		modal.setProcessingBody('<div class="text-center">Procesando...</div>');
		modal.setFooter('');
	},

	showProcessing: function(){
		modal.configProcessing();
		modal.show();
	}
}
