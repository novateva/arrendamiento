var excelTable = document.getElementById('excel-table');
var tbody = excelTable.getElementsByTagName('tbody')[0];
var unpaidSettlementsTable = document.getElementById('unpaid-settlements-table');
var settlementsTbody = unpaidSettlementsTable.getElementsByTagName('tbody')[0];
var rows = unpaidSettlementsTable.rows;

function addRowsToExcel(){

	var rowsToRemove = [];
	for(var i = 1; i < rows.length; i++){
		var row = rows[i];
		console.log(row);
		var selected = row.getElementsByClassName('excel-checkbox')[0].checked;

		if(!selected) continue;

		var excelRows = row.getElementsByClassName('excel-row');

		for(var j = 0; j < excelRows.length; j++){
			var tr = document.createElement('tr');
			tr.classList.add('text-center');
			var excelFields = excelRows[j].getElementsByClassName('excel-field');

			for(var k = 0; k < excelFields.length; k++){
				var td = document.createElement('td');
				td.innerHTML = excelFields[k].innerHTML;
				tr.appendChild(td)
			}
			tbody.appendChild(tr);
			rowsToRemove.push(row);
		}
	}

	for(var i = 0; i < rowsToRemove.length; i++){
		settlementsTbody.removeChild(rowsToRemove[i]);
	}
}
