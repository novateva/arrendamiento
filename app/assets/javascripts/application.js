// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.

//= require tooltip.js/dist/umd/tooltip.min
//= require popper.js/dist/umd/popper.min
//= require bootstrap/dist/js/bootstrap.min
//= require bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min
//= require bootstrap-daterangepicker/daterangepicker
//= require bootstrap-timepicker/js/bootstrap-timepicker.min
//= require cleave.js/dist/cleave.min
//= require codemirror/lib/codemirror
//= require nicescroll/dist/jquery.nicescroll.min
//= require moment/moment


//= require jquery-sparkline/jquery.sparkline.min
//= require chart.js/dist/Chart.min
//= require owl.carousel/dist/owl.carousel.min
//= require summernote/dist/summernote.min
//= require chocolat/dist/js/jquery.chocolat.min
//= require stisla
//= require scripts
//= require custom
//= require zipcelx/lib/standalone
//= require table_ordering.js

// Helper js functions to make available everywhere and avoid re-declaration
//= require_tree ./application
