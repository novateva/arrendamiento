require 'net/http'

module ApplicationHelper
    def current_class?(test_path)
      return 'active' if request.path == test_path
      ''
    end


    def link_to_add_row(name, f, association, **args)
      new_object = f.object.send(association).klass.new
      id = new_object.object_id
      fields = f.simple_fields_for(association, new_object, child_index: id) do |builder|
        render(association.to_s.singularize, f: builder)
      end
      link_to(name, '#', class: "add_fields " + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
    end

    def format_date date
      if date
        date.strftime('%d/%m/%Y')
      else
        '-/-/-'
      end
    end

    def show_errors(object, field_name)
		if object.errors.any?
			if !object.errors.messages[field_name].blank?
				object.errors.messages[field_name].join(", ")
			end
		end
	end

	def get_UF
		begin
			uri = URI("https://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=f39f3e325687640ecdcb805f113c0f9ef730b74f&formato=JSON")
			response = Net::HTTP.get_response(uri)
			json = JSON.parse response.body

			uf = json['UFs'][0]['Valor']
		rescue
			uf = 'No se pudo obtener'
		end

		return uf
	end

	def get_IPC(year, month)
		begin
			uri = URI("https://api.sbif.cl/api-sbifv3/recursos_api/ipc/#{year}/#{month}?apikey=f39f3e325687640ecdcb805f113c0f9ef730b74f&formato=JSON")
			response = Net::HTTP.get_response(uri)
			response_code = response.code
			ipc = 0
			if response.code == '200'
				json = JSON.parse response.body
				ipc = json['IPCs'][0]['Valor'].to_f
			end
		rescue
			ipc = 0
		end

		return ipc
	end

	def fmt_number(number)

		# Round to 2 decimals
		number = number.round(2)

		# If after that it is integer-like, transform to integer
		number = number == number.to_i ? number.to_i : number

		# Format delimiter and separator
		number_with_delimiter(number, delimiter: '.', separator: ',')
	end

end
