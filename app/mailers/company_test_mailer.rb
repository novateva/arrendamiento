class CompanyTestMailer < ApplicationMailer

    def  started_diagnostic(company_test)
        @company_test = company_test
        @user = company_test.user

        mail to: @user.email, 
            subject: "“#{@user.razon_social}” ha comenzado el diagnostico de Igualdad de Género y Conciliación"
    end

    def  completed_diagnostic(company_test)
        @company_test = company_test
        @user = company_test.user

        mail to: @user.email, 
            subject: "Termino de Diagnostico de empresa  “#{@user.razon_social}”"
    end

    def observation_added(user)
        @user = user

        mail to: @user.email,
            subject: "Acreditación de evidencia."
    end

    def assigned_improvement(company_test)
        @company_test = company_test
        @user = company_test.user

        mail to: @user.email,
            subject: "Comienzo de Implementación de la NCh-3262"
    end

    def answered_observation(company_test)
        @company_test = company_test
        @user = company_test.user

        mail to: @user.email,
            subject: "Resultado de evaluación de evidencias de Igualdad de Género y Conciliación"
    end
end
