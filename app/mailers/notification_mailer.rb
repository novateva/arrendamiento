class NotificationMailer < ApplicationMailer

	default from: 'notificaciones.civitanic@gmail.com'

	def payment(payment)

		email = payment.user.email
		@payment = payment

		attachments['comprobante.pdf'] = @payment.receipt.blob.download

		mail to: email, subject: "Comprobante de Pago de Arriendo"
	end

	def rent_expiration(admin_number)
		@tenant = admin_number.tenant
		@contract = admin_number.contract
		@property = admin_number.property

		email = @tenant.email

		mail to: email, subject: "Vencimiento de arriendo"
	end

	def rent_expired(admin_number)
		@tenant = admin_number.tenant
		@contract = admin_number.contract
		@property = admin_number.property

		email = @tenant.email

		mail to: email, subject: "Aviso de vencimiento de pago <%= @property.address %>"
	end

	def expiring_contract(contracts)
		@contracts = contracts

		emails = User.where(role: [:office_chief, :secretary, :admin]).pluck(:email)

		mail to: emails, subject: 'Renovación de Contratos'
	end

	def rent_readjustment(admin_number, readjustment)

		@tenant = admin_number.tenant
		@contract = admin_number.contract
		@readjustment = readjustment
		@property = admin_number.property

		mail to: @tenant.email, subject: 'Reajuste de Arriendo'

	end

	def send_password(user, password)
		@user = user
		@password = password

		mail to: @user.email, subject: 'Contraseña de su perfil'
	end

	def restore_password(new_password, email)
		@new_password = new_password

		mail to: email, subject: 'Recuperacion de Password'
	end
end
