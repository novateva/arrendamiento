class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.notification_publication.subject
  #
  def notification_publication(publication)
    @publication = publication
    @greeting = "Hi"

    emails = publication.company.users.collect(&:email).join(",")

    mail to: emails, cc: " reclutamiento@gesproa.cl", subject: "Resultados de Requerimiento Cargo  #{@publication.cargo}"

  end

  def register(user)
    @user = user
    
    mail to: @user.email, 
            subject: "Inscripción de empresa  “#{user.razon_social}” al diagnostico de Igualdad de Género y Conciliación"
  end

  def notification_register(company, username, useremail, usercargo, usertelefono)
    @company = company
    @username = username
    @useremail = useremail
    @usercargo = usercargo
    @usertelefono = usertelefono
    @greeting = "Hi"

    mail to: "reclutamiento@gesproa.cl", subject: "Nueva Empresa se Registró – #{@company.razon_social}"

  end

  def notification_recruitment(publication)
    @publication = publication
    @greeting = "Hi"
    
    emails = publication.company.users.collect(&:email).join(",")

    mail to: emails, cc: " reclutamiento@gesproa.cl", subject: "Solicitud de Requerimiento Cargo #{@publication.cargo}"

  end

  
end
