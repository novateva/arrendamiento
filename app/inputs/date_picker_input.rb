class DatePickerInput < SimpleForm::Inputs::Base

	def input(wrapper_options)
		template.content_tag(:div, class: 'input-group date') do
			template.concat @builder.date_field(attribute_name, input_html_options)
		end
	end

	def input_html_options
		father_options = super.deep_dup
		father_options[:class] = father_options[:class] | ['form-control']
		return father_options
	end
end
