class Settlement < ActiveRecord::Base

	belongs_to :contract

	validates_presence_of :contract_id, :month, :year, :invoice_number

	include Month

	# File handling
	has_one_attached :invoice
	has_one_attached :transference
	has_one_attached :receipt
	include FileValidation
	validate {
		file_validation(:invoice)
		file_validation(:transference)
		file_validation(:receipt)
	}

	after_rollback {
		file_delete(:invoice)
		file_delete(:transference)
		file_delete(:receipt)
	}

	def amount_to_transfer
		# Start the amount by the neat rent
		amount = self.contract.rent

		# Search expenses for this settlement that were not made by the
		# proprietary
		#expenses = Expense
		#	.where(property_id: self.contract.administration_number.property.id)
		#	.where('extract(year from date) = ?', self.year)
		#	.where('extract(month from date) = ?', Settlement.months[self.month])
		#	.where(proprietary: false)

		# Substract all of those expenses from the rent
		#expenses.map{ |e| amount -= e.amount }

		amount -= self.discount

		return amount
	end

	def discount
		discount = 0
		# Search expenses for this settlement that were not made by the
		# proprietary
		propertyHasExpenses = PropertyHasExpense
			.where(property_id: self.contract.administration_number.property.id)

		expense_amounts = propertyHasExpenses.map do |phe|
			if phe.expense.date.year == self.year and
			   phe.expense.date.month == Settlement.months[self.month] and
			   phe.expense.proprietary == false
				phe.expense.amount
			else
				0
			end
		end

		# Calcuate discuount by adding the amount of expenses payed by the tenant
		expense_amounts.each do |ea|
			discount += ea
		end

		return discount
	end

	def invoice_path
		ActiveStorage::Blob.service.send(:path_for, self.invoice.key)
	end

	# Returs string with the range covered by the settlement
	def range
		date1 = Date.new(self.year, self.month_number, self.contract.start_date.day)
		date2 = date1 + 1.month

		"#{date1.strftime('%d-%m-%Y')} hasta #{date2.strftime('%d-%m-%Y')}"
	end

	def generate_receipt_pdf(params = {})
		pdf_string = WickedPdf.new.pdf_from_string(
			#ActionController::Base.new().render_to_string(
			ApplicationController.new.render_to_string(
				:template => 'settlements/settlement_pdf_template',
				:locals => {
					:settlement => self,
					:contract => self.contract,
					:proprietary => self.contract.administration_number.proprietary,
					:tenant => self.contract.administration_number.tenant,
					:property => self.contract.administration_number.property,
					:admin_n => self.contract.administration_number,
				},
				:layout => 'receipt',
			),

			#:pdf => estimate_number,
			:layout => false,
			:page_size => 'Letter',
			:lowquality => false,
			:handlers => [:erb],
			:formats => [:html],
			#:margin => {:top                => 5,
			#			:bottom             => 0,
			#			:left               => 0,
			#			:right              => 0},
			:orientation => 'Portrait',
			:disposition => 'attachment'
		)

		tempfile = Tempfile.new(["temp", ".pdf"], Rails.root.join('tmp'))
		tempfile.binmode
		tempfile.write pdf_string
		tempfile.close

		self.receipt.attach(io: File.open(tempfile.path), filename: 'liquidacion.pdf' )

		tempfile.unlink
	end

end
