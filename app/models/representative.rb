class Representative < ActiveRecord::Base
	belongs_to :user

	# Needs to be wrapped to avoid required in form
	#validate {validates_presence_of :name, :rut, :telephone, :email}
	validate {validates_presence_of :name, :rut}
	validate :validate_user_role

	validates :email,
		format: { with: URI::MailTo::EMAIL_REGEXP },
		if: Proc.new {|representative| not representative.email.strip.empty? }

	def validate_user_role
		unless self.user.role.to_sym.in? [:tenant, :proprietary]
			self.errors.add :user, "Why would you try to break me?"
		end
	end
end
