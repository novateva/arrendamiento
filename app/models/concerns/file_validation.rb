module FileValidation
	extend ActiveSupport::Concern

	included do
		def file_validation(file_field)
			# Add message if no file was uploaded while being created
			if not self.persisted? and not self.send(file_field).attached?
				self.errors.add(file_field, 'Debe subir un archivo')
			end
		end

		# This is meant to be called after rollback
		def file_delete(file_field)
			# If the file was being created (doesnt yet have id)
			if not self.persisted?
				self.send(file_field).purge
			end
		end
	end
end
