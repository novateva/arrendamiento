module BankAccount
	extend ActiveSupport::Concern

	BANK_STR = {
		:banco_de_chile => 'Banco de Chile',
		:banco_internacional => 'Banco Internacional',
		:scotiabank_chile => 'Scotiabank Chile',
		:banco_de_credito_e_inversiones => 'Banco de Crédito e Inversiones',
		:corpbanca => 'Corpbanca',
		:banco_bice => 'Banco Bice',
		:hsbc_bank_chile => 'HSBC Bank (Chile)',
		:banco_santander => 'Banco Santander',
		:banco_itau_chile => 'Banco Itaú Chile',
		:banco_security => 'Banco Security',
		:banco_falabella => 'Banco Falabella',
		:deutsche_bank => 'Deutsche Bank',
		:banco_ripley => 'Banco Ripley',
		:rabobank_chile => 'Rabobank Chile',
		:banco_consorcio => 'Banco Consorcio',
		:banco_penta => 'Banco Penta',
		:banco_paris => 'Banco Paris',
		:banco_estado => 'Banco Estado',
	}

	ACCOUNT_TYPE_STR = {
		:rut => 'Rut',
		:savings => 'Ahorro',
		:checking => 'Corriente',
	}

	included do

		enum bank: BANK_STR.keys

		enum account_type: ACCOUNT_TYPE_STR.keys

		def bank_string
			BANK_STR[self.bank.to_sym]
		end

		def account_type_string
			ACCOUNT_TYPE_STR[self.account_type.to_sym]
		end

		def self.bank_collection
			BANK_STR.invert
		end

		def self.account_type_collection
			ACCOUNT_TYPE_STR.invert
		end

	end
end
