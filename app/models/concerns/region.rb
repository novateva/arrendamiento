module Region extend ActiveSupport::Concern

	REGION_STR = {
		#:r_tarapaca => 'Tarapacá',
		#:r_antofagasta => 'Antofagasta',
		#:r_atacama => 'Atacama',
		#:r_coquimbo => 'Coquimbo',
		#:r_valparaiso => 'Valparaíso',
		#:r_libertador_general_bernardo_o_higgins => 'Libertador General Bernardo O’Higgins',
		#:r_maule => 'Maule',
		#:r_biobio => 'Biobío',
		#:r_la_araucania => 'La Araucanía',
		#:r_los_lagos => 'Los Lagos',
		#:r_aysen_del_general_carlos_ibanez_del_campo => 'Aysén del General Carlos Ibáñez del Campo',
		#:r_magallanes_y_antartica_chilena => 'Magallanes y Antártica Chilena',
		#:r_metropolitana_de_santiago => 'Metropolitana de Santiago',
		#:r_los_rios => 'Los Ríos',
		#:r_arica_y_parinacota => 'Arica y Parinacota',
		#:r_nuble => 'Ñuble'
		:r_tarapaca => 'Región de Tarapacá',
		:r_antofagasta => 'Región de Antofagasta',
		:r_atacama => 'Región de Atacama',
		:r_coquimbo => 'Región de Coquimbo',
		:r_valparaiso => 'Región de Valparaíso',
		:r_del_libertador_gral__bernardo_o_higgins => 'Región del Libertador Gral. Bernardo O\'Higgins',
		:r_del_maule => 'Región del Maule',
		:r_del_biobio => 'Región del Biobío',
		:r_la_araucania => 'Región de la Araucanía',
		:r_los_lagos => 'Región de Los Lagos',
		:r_aisen_del_gral__carlos_ibanez_del_campo => 'Región Aisén del Gral. Carlos Ibáñez del Campo',
		:r_magallanes_y_de_la_antartica_chilena => 'Región de Magallanes y de la Antártica Chilena',
		:r_los_rios => 'Región de Los Ríos',
		:r_arica_y_parinacota => 'Arica y Parinacota',
		:r_metropolitana => 'Region Metropolitana',
	}

	included do
		enum region: REGION_STR.keys

		def region_string
			REGION_STR[self.region.to_sym]
		end

		def self.region_collection
			REGION_STR.invert
		end
	end
end
