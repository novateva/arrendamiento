module Month extend ActiveSupport::Concern

	MONTH_TO_STR = {
		:january => 'Enero',
		:february => 'Febrero',
		:march => 'Marzo',
		:april => 'Abril',
		:may => 'Mayo',
		:june => 'Junio',
		:july => 'Julio',
		:august => 'Agosto',
		:september => 'Septiembre',
		:october => 'Octubre',
		:november => 'Noviembre',
		:december => 'Diciembre'
	}

	included do
		enum month: {
			:january => 1,
			:february => 2,
			:march => 3,
			:april => 4,
			:may => 5,
			:june => 6,
			:july => 7,
			:august => 8,
			:september => 9,
			:october => 10,
			:november => 11,
			:december => 12
		}

		def month_string(month = nil)
			if(not month)
				MONTH_TO_STR[self.month.to_sym]
			else
				MONTH_TO_STR[self.class.months.invert[month].to_sym]
			end
		end

		def month_number
			self.class.months[self.month]
		end

		def self.month_collection
			MONTH_TO_STR.invert
		end

	end
end
