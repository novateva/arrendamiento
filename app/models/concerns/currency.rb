module Currency
	extend ActiveSupport::Concern

	CURRENCY_STR = {
		:peso => 'Peso',
		:uf   => 'UF'
	}

	included do
		enum currency: CURRENCY_STR.keys

		def currency_string
			CURRENCY_STR[self.currency.to_sym]
		end

		def self.currency_collection
			CURRENCY_STR.invert
		end
	end
end
