class AdministrationBeneficiary < ActiveRecord::Base

	validates_presence_of :bank, :account_type, :account_number,
						  :titular, :percentage, :email, :rut

	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }

	include BankAccount

	def verification_digit
		self.rut[-1]
	end
end
