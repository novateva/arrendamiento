class Property < ActiveRecord::Base
	# Associations
	has_many :property_has_expenses, dependent: :destroy
	has_many :expenses, through: :property_has_expenses, dependent: :destroy
	has_many :property_works, dependent: :destroy
	has_one :common_expenses_administrator, dependent: :destroy
	has_many :administration_beneficiaries, dependent: :destroy
	has_one :administration_number

	accepts_nested_attributes_for :common_expenses_administrator
	accepts_nested_attributes_for :administration_beneficiaries, allow_destroy: true

	# Validations
	validates_presence_of :property_type, :region, :commune, :water_ias,
						  :electricity_ias, :gas_ias, :address,
						  :adm_percentage, :administration_number
	validates_inclusion_of :common_expenses, in: [true, false]
	validates_inclusion_of :adm_rent, in: [true, false]
	validates_inclusion_of :adm_contributions, in: [true, false]
	validates_inclusion_of :adm_common_expenses, in: [true, false]
	validate{
		if not (self.adm_rent or self.adm_contributions or
				self.adm_common_expenses or self.adm_other.strip.length != 0)
			error_str = 'Debe elegir almenos una'
			self.errors.add(:adm_rent, error_str)
			self.errors.add(:adm_contributions, error_str)
			self.errors.add(:adm_common_expenses, error_str)
			self.errors.add(:adm_other, error_str)
		end
	}

	validates_associated :common_expenses_administrator
	validates_associated :administration_beneficiaries
	validate :common_expenses_administrator_inclusion

	# Mandate file
	has_one_attached :mandate
	include FileValidation
	#validate {
	#	file_validation(:mandate)
	#}

	after_rollback {
		file_delete(:mandate)
	}

	# Enums
	include Commune
	include Region

	# Property Type
	TYPE_STR = {
		:office		 => 'Oficina',
		:local		 => 'Local',
		:site		 => 'Sitio',
		:house		 => 'Casa',
		:apartment	 => 'Departamento',
		:parking_lot => 'Estacionamiento',
		:industrial  => 'Industrial',
		:cellar		 => 'Bodega',
		:shed		 => 'Galpon',
	}
	enum property_type: TYPE_STR.keys

	# State
	STATE_STR = {
		:available		=> 'Disponible',
		:rented			=> 'Arrendada',
		:for_repairment => 'Por Reparacion',
	}
	enum state: STATE_STR.keys

	def type_string
		TYPE_STR[self.property_type.to_sym]
	end

	def state_string
		STATE_STR[self.state.to_sym]
	end

	def self.type_collection
		TYPE_STR.invert
	end

	def self.state_collection
		STATE_STR.invert
	end

	def self.common_expenses_collection
		{
			'No' => false,
			'Si' => true
		}
	end

	def contributions_amount
		self.property_role_contribution + self.cellar_role_contribution + self.parking_role_contribution
	end

	def active_property_works
		PropertyWork.where(property: self, finished: false)
	end

	private

		def common_expenses_administrator_inclusion
			has_administrator = false
			if self.common_expenses_administrator
				has_administrator = true
			end

			if self.common_expenses == (not has_administrator)
				if self.common_expenses
					self.errors.add(:common_expenses_administrator, message: "Debe tener administracion de gastos comunes")
				else
					self.errors.add(:common_expenses_administrator, message: "Tratando de romper mi sistema?")
				end
			end
		end
end
