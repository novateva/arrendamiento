class Inventory < ActiveRecord::Base

	belongs_to :contract

	has_one_attached :file
	include FileValidation
	validate { file_validation(:file) }
	after_rollback{ file_delete(:file) }
end
