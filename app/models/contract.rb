class Contract < ActiveRecord::Base
	has_many :payments, dependent: :destroy
	has_many :settlements, dependent: :destroy
	has_many :readjustments, dependent: :destroy
	has_one :administration_number

	has_many :contract_expenses, dependent: :destroy
	accepts_nested_attributes_for :contract_expenses

	has_many :inventories, dependent: :destroy
	has_many :discharges, dependent: :destroy

	validates_presence_of :expiration_notice, :start_date,
						  :expiration_date, :daily_penalty_percentage,
						  :days_for_penalty, :rent, :currency,
						  :readjustment_term, :readjustment_type,
						  :warranty, :warranty_payment, :amount_of_fees,
						  :amount_of_readjustments
	validates_inclusion_of :active, in: [true, false]

	validates :daily_penalty_percentage, numericality: {greater_than_or_equal_to: 0}
	validates :days_for_penalty, numericality: {greater_than_or_equal_to: 0}
	validates :rent, numericality: {greater_than_or_equal_to: 0}
	validates :warranty, numericality: {greater_than_or_equal_to: 0}
	validates :amount_of_fees, numericality: {greater_than_or_equal_to: 1, lesser_than_or_equal_to: 12}
	validates :amount_of_readjustments, numericality: {grater_than_or_equal_to: 1}
	validate  :dates_make_sense

	# Termination validation
	validates_presence_of :termination_date, if: :terminating?

	# Readjustments
	accepts_nested_attributes_for :readjustments, allow_destroy: true
	validates_associated :readjustments
	validate :readjustment_dates_make_sense

	# Warranty file
	has_one_attached :warranty_receipt
	has_one_attached :proprietary_payment
	include FileValidation
	after_rollback {
		file_delete(:warranty_receipt)
		file_delete(:proprietary_payment)
	}

	def generate_warranty_receipt_pdf(params = {})
		pdf_string = WickedPdf.new.pdf_from_string(
			#ActionController::Base.new().render_to_string(
			ApplicationController.new.render_to_string(
				:template => 'payments/warranty_receipt_template',
				:locals => {
					:contract => self,
					:property => self.administration_number.property,
					:tenant => self.administration_number.tenant,
					:proprietary => self.administration_number.proprietary,
				},
				:layout => 'receipt',
			),

			:layout => false,
			:page_size => 'Letter',
			:lowquality => false,
			:handlers => [:erb],
			:formats => [:html],
			:orientation => 'Portrait',
			:disposition => 'attachment'
		)

		tempfile = Tempfile.new(["temp", ".pdf"], Rails.root.join('tmp'))
		tempfile.binmode
		tempfile.write pdf_string
		tempfile.close

		self.warranty_receipt.attach(io: File.open(tempfile.path), filename: 'garantia.pdf' )

		tempfile.unlink
	end

	def generate_proprietary_payment_pdf(payment = nil, settlement = nil, prior_balance = 0)
		if payment.nil?
			payment = Payment.where(contract: settlement.contract, year: settlement.year, month: settlement.month).first
		elsif settlement.nil?
			settlement = Settlement.where(contract: payment.contract, year: payment.year, month: payment.month).first
		else
			raise 'Payment and settlement can\'t be null at the same time'
		end

		pdf_string = WickedPdf.new.pdf_from_string(
			#ActionController::Base.new().render_to_string(
			ApplicationController.new.render_to_string(
				:template => 'payments/proprietary_payment_template',
				:locals => {
					:property => self.administration_number.property,
					:tenant => self.administration_number.tenant,
					:proprietary => self.administration_number.proprietary,
					:contract => self,
					:admin_n => self.administration_number,
					:payment => payment,
					:settlement => settlement,
					:prior_balance => prior_balance,
				},
				:layout => 'receipt',
			),

			#:pdf => estimate_number,
			:layout => false,
			:page_size => 'Letter',
			:lowquality => false,
			:handlers => [:erb],
			:formats => [:html],
			#:margin => {:top                => 5,
			#			:bottom             => 0,
			#			:left               => 0,
			#			:right              => 0},
			:orientation => 'Portrait',
			:disposition => 'attachment'
		)

		tempfile = Tempfile.new(["temp", ".pdf"], Rails.root.join('tmp'))
		tempfile.binmode
		tempfile.write pdf_string
		tempfile.close

		self.proprietary_payment.attach(io: File.open(tempfile.path), filename: 'pago_propietario.pdf' )

		tempfile.unlink
	end

	# Currency
	include Currency

	# expiraton notice
	EXPIRATION_NOTICE_STR = {
		:days30  => '30 Días',
		:days60  => '60 Días',
		:days90  => '90 Días',
		:days120 => '120 Días'
	}
	enum expiration_notice: EXPIRATION_NOTICE_STR.keys

	def expiration_notice_string
		EXPIRATION_NOTICE_STR[self.expiration_noice.to_sym]
	end

	def self.expiration_notice_collection
		EXPIRATION_NOTICE_STR.invert
	end

	# readjustment_type
	READJUSTMENT_TYPE_STR = {
		#:rtype_peso => 'Peso',
		:rtype_uf   => 'UF',
		:rtype_ipc  => 'IPC',
	}
	enum readjustment_type: READJUSTMENT_TYPE_STR.keys

	def readjustment_type_string
		READJUSTMENT_TYPE_STR[self.readjustment_type.to_sym]
	end

	def self.readjustment_type_collection
		READJUSTMENT_TYPE_STR.invert
	end

	# readjustment_term
	READJUSTMENT_TERM_STR = {
		:no_readjustment => 'Sin Reajuse',
		:months3		 => '3 meses',
		:months6		 => '6 meses',
		:months12		 => '12 meses'
	}
	enum readjustment_term: READJUSTMENT_TERM_STR.keys

	def readjustment_term_string
		READJUSTMENT_TERM_STR[self.readjustment_term.to_sym]
	end

	def self.readjustment_term_collection
		READJUSTMENT_TERM_STR.invert
	end

	# warranty_payment
	WARRANTY_PAYMENT_STR = {
		:total		   => 'Pago Total',
		:equal_fees	   => 'Cuotas Iguales',
		:variable_fees => 'Cuotas Variables',
	}
	enum warranty_payment: WARRANTY_PAYMENT_STR.keys

	def warranty_payment_string
		WARRANTY_PAYMENT_STR[self.warranty_payment.to_sym]
	end

	def self.warranty_payment_collection
		WARRANTY_PAYMENT_STR.invert
	end

	# amount_of_fees
	AMOUNT_OF_FEES_MAP = {
		'1'  => 1,
		'2'  => 2,
		'3'  => 3,
		'4'  => 4,
		'5'  => 5,
		'6'  => 6,
		'7'  => 7,
		'8'  => 8,
		'9'  => 9,
		'10' => 10,
		'11' => 11,
		'12' => 12,
	}
	def self.amount_of_fees_collection
		AMOUNT_OF_FEES_MAP
	end

	def state_string
		if self.active
			'Vigente'
		else
			'Vencido'
		end
	end

	def signal_termination
		@terminating = true
	end

	def terminating?
		@terminating
	end

	def dates_make_sense
		if (not self.start_date.nil?) and (not self.expiration_date.nil?) and self.expiration_date < start_date
			self.errors.add(:expiration_date, message: "La fecha de termino no puede ser anterior a la fecha de inicio")
		end
	end

	def readjustment_dates_make_sense

		if self.readjustments[0].until.nil? or self.start_date.nil? or self.readjustments[0].until < self.start_date
			self.errors.add(:readjustments, message: "Fechas invalidas")
		end

		for i in 1...self.amount_of_readjustments
			if self.readjustments[i].until.nil? or self.readjustments[i-1].until.nil? or self.readjustments[i].until < self.readjustments[i-1].until
				self.errors.add(:readjustments, message: "Fechas invalidas")
			end
		end

	end

	def administration_percentage
		self.administration_number.property.adm_percentage * self.rent / 100
	end

	def expiration_notice_date
		if self.expiration_notice.to_sym == :days30
			self.expiration_date - 30.days
		elsif self.expiration_notice.to_sym == :days60
			self.expiration_date - 60.days
		elsif self.expiration_notice.to_sym == :days90
			self.expiration_date - 90.days
		elsif self.expiration_notice.to_sym == :days120
			self.expiration_date - 120.days
		end
	end

	def readjusted_rent(year, month)
		rent = self.rent
		#date_limit = Date.new(year, month, Date.today.day)
		date_limit = Date.new(year, month, self.start_date.day)
		self.readjustments.order(:until).each_with_index do |readjustment, i|
			if readjustment.until <= date_limit
				rent += readjustment.amount
			elsif i > 0 and readjustments[i-1].until < date_limit and date_limit <= readjustment.until
				rent += readjustment.amount
			end
		end

		return rent
	end

	def readjusted_rent_date(date)
		self.readjustments.each_with_index do |readjustment, i|
			if readjustment.until >= date
				return self.rent + self.readjustments[0..i].pluck(:amount).sum
			end
		end

		return self.rent + self.readjustments.pluck(:amount).sum
	end

	def previous_readjusted_rent(date)
		self.readjustments.each_with_index do |readjustment, i|
			if readjustment.until >= date
				return self.rent + self.readjustments[0...i].pluck(:amount).sum
			end
		end

		return self.rent + self.readjustments.pluck(:amount).sum
	end

	def rent_readjustment(date)
		rent_readjustment = 0
		#date_limit = Date.new(date.year, date.month, 1)
		date_limit = Date.new(date.year, date.month, self.start_date.day)
		self.readjustments.order(:until).each_with_index do |readjustment, i|
			if readjustment.until <= date_limit
				rent_readjustment += readjustment.amount
			elsif i > 0 and readjustments[i-1].until < date_limit and date_limit <= readjustment.until
				rent_readjustment += readjustment.amount
			end
		end

		return rent_readjustment
	end

	def penalty(date)
		# Get ammount of delayed days
		delayed_days = (Date.today - date)

		# See if it is still within the acceptance bound
		days_outside_bound = (delayed_days - self.days_for_penalty)
		days_outside_bound = days_outside_bound < 0 ? 0 : days_outside_bound

		# Return amount of days by the applied penalty percentage
		days_outside_bound * ((self.daily_penalty_percentage/100) * self.readjusted_rent(date.year, date.month))
	end

	def payment_day(date = Date.today)
		Date.new(year = date.year, month = date.month, day = self.start_date.day)
	end

	def readjustment_period(date)
		readjustments = self.readjustments.order(:until)
		period_string = ''
		readjustments.each_with_index do |readjustment, i|
			lower_limit = i == 0 ? self.start_date : readjustments[i-1].until
			upper_limit = readjustment.until
			if lower_limit <= date and date < upper_limit
				period_string  = "#{lower_limit} a #{upper_limit}"
			end
		end

		return period_string
	end

end
