class Company < ActiveRecord::Base

	include Region
	include Commune

	include BankAccount

	def self.info
		Company.all.last
	end
end
