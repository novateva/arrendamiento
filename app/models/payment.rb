class Payment < ActiveRecord::Base
	belongs_to :user
	belongs_to :contract
	#has_one :settlement, through: :settlement_has_payment

	validates_presence_of :user_id, :contract_id, :date, :amount, :payment_method, :month, :year

	# Payment Method
	PAYMENT_METHOD_STR = {
		:cash => 'Efectivo',
		:transference => 'Transferencia',
		:cash_deposit => 'Depósito efectivo',
		:check_deposit => 'Depósito cheque',
		:day_check => 'Cheque la día',
		:date_check => 'Cheque a fecha'
	}
	enum payment_method: PAYMENT_METHOD_STR.keys

	def payment_method_string
		PAYMENT_METHOD_STR[self.payment_method.to_sym]
	end

	def self.payment_method_collection
		PAYMENT_METHOD_STR.invert
	end

	include Month

	# File handling
	has_one_attached :receipt
	include FileValidation
	validate {
		file_validation(:receipt)
	}

	after_rollback {
		file_delete(:receipt)
	}

	def delay_days
		days_late = self.date.day - self.contract.start_date.day
		days_late < self.contract.days_for_penalty ? 0 : days_late - self.contract.days_for_penalty
	end

	def delay_penalty
		self.delay_days * self.contract.daily_penalty_percentage
	end

	# Fetch expenses that the tenant paid
	def discounts
		year = Date.today.year
		month = self.month

		admin_number = self.contract.administration_number
		discounts = PropertyHasExpense.where(property: admin_number.property)
			.select {|phe|
				phe.expense.date.year == year and
				phe.expense.date.month == month and
				phe.expense.proprietary == false
			}.map {|phe| phe.expense.amount }
			.sum

		return discounts
	end

	def generate_receipt_pdf(prior_balance)
		pdf_string = WickedPdf.new.pdf_from_string(
			ApplicationController.new.render_to_string(
				:template => 'payments/payment_receipt_template',
				:locals => {
					:property => self.contract.administration_number.property,
					:tenant => self.user,
					:contract => self.contract,
					:payment => self,
					:prior_balance => prior_balance,
				},
				:layout => 'receipt',
			),

			#:pdf => estimate_number,
			:layout => false,
			:page_size => 'Letter',
			:lowquality => false,
			:handlers => [:erb],
			:formats => [:html],
			#:margin => {:top                => 5,
			#			:bottom             => 0,
			#			:left               => 0,
			#			:right              => 0},
			:orientation => 'Portrait',
			:disposition => 'attachment'
		)

		tempfile = Tempfile.new(["temp", ".pdf"], Rails.root.join('tmp'))
		tempfile.binmode
		tempfile.write pdf_string
		tempfile.close

		self.receipt.attach(io: File.open(tempfile.path), filename: 'arriendo.pdf' )

		tempfile.unlink
	end

	def availability_date
		if self.check_deposit? or self.day_check? or self.date_check?
			self.date + 3.days
		else
			self.date + 2.days
		end
	end

	def self.with_unpaid_settlement
		unpaid_settlements = []
		AdministrationNumber.active_contract.each do |n|

			ls_year, ls_month = n.last_settlement_date
			payments = Payment
				.where(contract: n.contract)
				.where('make_date(payments.year, payments.month, 1) > DATE(?)', Date.new(ls_year, ls_month, 1))

			unpaid_settlements = unpaid_settlements.empty? ? payments : unpaid_settlements + payments

		end

		return unpaid_settlements
	end

end
