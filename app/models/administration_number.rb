class AdministrationNumber < ActiveRecord::Base

	belongs_to :property, 						 optional: true
	belongs_to :proprietary, class_name: 'User', optional: true
	belongs_to :tenant, 	 class_name: 'User', optional: true
	belongs_to :contract, 						 optional: true

	def self.without_proprietary
		AdministrationNumber.where(proprietary: nil)
	end

	def self.without_proprietary_collection
		available_numbers = AdministrationNumber.where(proprietary: nil)
		available_numbers_collection = {}
		available_numbers.each do |n|
			available_numbers_collection[n.id.to_s] = n.id
		end

		return available_numbers_collection
	end

	def self.without_tenant
		AdministrationNumber.where.not(proprietary: nil).where.not(property: nil).where(tenant: nil)
	end

	def self.without_contract
		AdministrationNumber.where.not(proprietary: nil, property: nil, tenant: nil).where(contract: nil)
	end

	def self.without_contract_collection
		numbers = self.without_contract
		numbers_collection = {}
		numbers.each do |n|
			numbers_collection[n.id.to_s] = n.id
		end

		return numbers_collection
	end

	def self.active_contract
		#AdministrationNumber.where.not(proprietary: nil, property: nil, tenant: nil, contract: nil).where(contract.active: true)
		AdministrationNumber.where.not(proprietary: nil, property: nil, tenant: nil, contract: nil)
			.joins(:contract).where(contracts: {active: true})
	end

	def has_proprietary?
		self.proprietary != nil
	end

	def has_tenant?
		self.tenant != nil
	end

	def release_property
		self.property = nil
		self.proprietary = nil
		self.tenant = nil
		self.contract = nil
		self.save
	end

	def last_payment_date
		last_payment = Payment.where(contract: self.contract).order(:year).order(:month).last
		if last_payment == nil
			last_payment_month = self.contract.start_date.month
			last_payment_year = self.contract.start_date.year
		else
			last_payment_month = last_payment.month_number
			last_payment_year = last_payment.year
		end

		return last_payment_year, last_payment_month
	end

	def	delayed_months
		last_payment_year, last_payment_month = self.last_payment_date
		n_months = (Date.today.year - last_payment_year)*12 + (Date.today.month - last_payment_month)
		n_months >= 0 ? n_months : 0
	end

	def last_settlement_date
		last_settlement = Settlement.where(contract: self.contract).order(:year).order(:month).last
		if last_settlement.nil?
			tmp_date = self.contract.start_date - 1.month
			last_settlement_month = tmp_date.month
			last_settlement_year = tmp_date.year
		else
			last_settlement_month = last_settlement.month_number
			last_settlement_year =  last_settlement.year
		end

		return last_settlement_year, last_settlement_month
	end

	def delayed_settlements
		last_settlement_year, last_settlement_month = self.last_settlement_date
		(Date.today.year - last_settlement_year)*12 + (Date.today.month - last_settlement_month)
	end

	def settlements
		Settlement.where(contract: self.contract)
	end

	def payments
		Payment.where(contract: self.contract)
	end
end
