class Cosigner < ActiveRecord::Base

	belongs_to :user

	#validates_presence_of :name, :rut, :telephone, :address,
	#					  :region, :commune
	validate {validates_presence_of :name, :rut, :region, :commune}

	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, if: Proc.new {|user| (not user.email.nil?) and (not user.email.strip.empty?)}
	validates :email2, format: { with: URI::MailTo::EMAIL_REGEXP }, if: Proc.new {|user| (not user.email2.nil?) and (not user.email2.strip.empty?)}

	# Commune
	include Commune

	# Region
	include Region
end
