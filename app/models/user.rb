class User < ActiveRecord::Base

	# Relations
	has_many :payments, 			 dependent: :destroy
	has_many :notifications, 		 dependent: :destroy
	has_many :administration_numbers

	has_one :representative, dependent: :destroy
	has_one :cosigner, dependent: :destroy

	# Representative
	def attributes_empty(atts)
		atts.reduce(true) {|acc, (k, v)|
		acc and (['region', 'commune'].include? k or v.strip.empty?)}
	end
	accepts_nested_attributes_for :representative, allow_destroy: true,
								   reject_if: :attributes_empty

	# Cosigner
	accepts_nested_attributes_for :cosigner, allow_destroy: true,
								  reject_if: :attributes_empty

	# Password
	has_secure_password

	# Validation
	validates_presence_of :password, :email, :name, :rut,
						  :telephone, :address, :region, :commune,
						  :role, on: :create
	validates :rut, uniqueness: true
	validates :email, uniqueness: true
	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
	validates :email2, format: { with: URI::MailTo::EMAIL_REGEXP }, if: Proc.new {|user| (not user.email2.nil?) and (not user.email2.strip.empty?)}

	validates_associated :representative
	validates_associated :cosigner

	# Role
	enum role: [:admin, :secretary, :executive, :office_chief, :tenant, :proprietary, :guest]

	ROLE_STR = {
		:admin => 'Administrador',
		:secretary => 'Secretario',
		:executive => 'Ejecutivo',
		:office_chief => 'Jefe de Oficina',
		:tenant => 'Arrendatario',
		:proprietary => 'Propietario',
		:guest => 'Invitado'
	}

	# Commune
	include Commune

	# Region
	include Region

	def index_path
		"/users/#{self.role.pluralize}"
	end

	def role_string
		ROLE_STR[self.role.to_sym]
	end

	def internal?
		[:admin, :secretary, :executive, :office_chief].include? self.role.to_sym
	end

	def self.role_collection
		ROLE_STR.invert
	end

	def self.internal_role_collection
		ROLE_STR.invert.select do |key, value|
			[:admin, :secretary, :executive, :office_chief].include? value
		end
	end

	def administration_numbers
		if self.proprietary?
			AdministrationNumber.where(proprietary: self)
		elsif self.tenant?
			AdministrationNumber.where(tenant: self)
		else
			[]
		end
	end

	def get_contract
		admin_numbers = AdministrationNumber.where(tenant: self)
		contract = nil

		if not admin_numbers.empty?
			admin_number = admin_numbers[0]
			contract = admin_number.contract
		end

		contract
	end

	def can_be_deleted?
		if self.proprietary?
			Property.where(administration_number: self.administration_numbers).empty?
		elsif self.tenant?
			Contract.where(administration_number: self.administration_numbers, active: true).empty?
		else
			true
		end
	end

	def admin_numbers
		if self.tenant?
			admin_numbers = AdministrationNumber.where(tenant: self)
		elsif self.proprietary?
			admin_numbers = AdministrationNumber.where(proprietary: self)
		else
			admin_numbers = []
		end

		admin_numbers.reduce(''){|str, n| str + n.id.to_s + ', '}.chomp(', ')
	end

end
