class IpcReadjustment < ActiveRecord::Base

	include Month

	def self.for_year(year)
		IpcReadjustment.where(year: year)
	end

	def self.month_value_for_year(year)
		ipc_for_year = IpcReadjustment.for_year(year).order(:month)
		collection = {}
		ipc_for_year.each do |ipc|
			collection[ipc.month.to_s] = ipc.value
		end

		return collection

	end

	def self.for_month(year, month)
		IpcReadjustment.where(year: year, month: month).first.value
	end
end
