class Expense < ActiveRecord::Base
	# Realtions
	has_one :property_has_expense, dependent: :destroy
	has_one :property_work_has_expense, dependent: :destroy

	# Validation
	validates_presence_of :date, :amount, :description, :voucher_type
	validates_inclusion_of :proprietary, in: [true, false]
	validates :amount, numericality: {greater_than_or_equal_to: 0}

	# File handling
	has_one_attached :voucher
	include FileValidation
	#validate {
	#	file_validation(:voucher)
	#}

	after_rollback {
		file_delete(:voucher)
	}

	# Voucher Type Enum
	enum voucher_type: [:ticket, :invoice, :receipt, :contributions, :others]

	# Voucher and Voucher Number validation
	validate {
		if (self.voucher.attached? or self.voucher_number.present?)
			validates_presence_of :voucher_number
			file_validation(:voucher)
		end
	}

	VOUCHERTYPE_STR = {
		:ticket		   => 'Boleta',
		:invoice	   => 'Factura',
		:receipt	   => 'Recibo'
	}

	def voucher_type_string
		VOUCHERTYPE_STR[self.voucher_type.to_sym]
	end

	def voucher_path
		ActiveStorage::Blob.service.send(:path_for, self.voucher.key)
	end

	def assigned_to
		if self.proprietary
			'Propietario'
		else
			'Arrendatario'
		end
	end

	def administration_number
		self.property_has_expense.property.administration_number
	end

	def property
		if self.property_has_expense.nil?
			self.property_work_has_expense.property_work.property
		else
			self.property_has_expense.property
		end
	end

end
