class PropertyHasExpense < ActiveRecord::Base
	belongs_to :expense
	belongs_to :property
end
