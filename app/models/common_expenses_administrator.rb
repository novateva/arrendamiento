class CommonExpensesAdministrator < ActiveRecord::Base

	belongs_to :property
	validates_presence_of :name, :rut, :address, :region,
						  :commune, :bank, :account_type,
						  :account_number, :titular

	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, if: Proc.new {|cea| (not cea.email.nil?) and (not cea.email.strip.empty?)}
	validates :email2, format: { with: URI::MailTo::EMAIL_REGEXP }, if: Proc.new {|cea| (not cea.email2.nil?) and (not cea.email2.strip.empty?)}

	include Region
	include Commune
	include BankAccount

end
