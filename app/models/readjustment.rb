class Readjustment < ActiveRecord::Base
	belongs_to :contract

	validates_presence_of :contract, :until, :amount, :currency

	validates :amount, numericality: {greater_or_equal_than: 0}

	include Currency
end
