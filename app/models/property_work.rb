class PropertyWork < ActiveRecord::Base
	belongs_to :property
	has_many :property_work_has_expenses, dependent: :destroy
	has_many :expenses, through: :property_work_has_expenses, dependent: :destroy

	accepts_nested_attributes_for :expenses, allow_destroy: true
	validates_associated :expenses

	validates_presence_of :property, :description, :provider,
						  :start_date, :end_date


	def self.state_collection
		{
			'En Ejecución' => false,
			'Terminado' => true
		}
	end

	def total_expense
		self.expenses.reduce(0) {|sum, expense| sum + expense.amount}
	end


end
