class LogEntry < ActiveRecord::Base
	belongs_to :user

	validates_presence_of :user, :datetime, :action, :model, :description
end
