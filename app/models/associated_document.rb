class AssociatedDocument < ActiveRecord::Base
	belongs_to :user

	# File handling
	has_one_attached :document
	include FileValidation
	validate {
		file_validation(:document)
	}

	after_rollback {
		file_delete(:document)
	}
end
