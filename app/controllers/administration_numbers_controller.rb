class AdministrationNumbersController < ApplicationController
	before_action :index do authorize_read(AdministrationNumber) end

	def index
		@admin_numbers = AdministrationNumber
			.where.not(proprietary: nil)
			.where.not(property: nil)
	end
end
