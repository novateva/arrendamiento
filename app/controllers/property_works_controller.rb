class PropertyWorksController < ApplicationController
	before_action only: [:index, :show] do authorize_read(PropertyWork) end
	before_action except: [:index, :show] do authorize_manage(PropertyWork) end

	def index
		@admin_numbers = AdministrationNumber.active_contract
	end

	def show
		@property = Property.find(params[:property_id])
		@property_works = PropertyWork.where(property: @property)
	end

	def new
		@property_work = PropertyWork.new(property_id: params[:property_id])
		@property_work.expenses.build(date: Date.today)
	end

	def add_expense
		@dummy_property_work = PropertyWork.new
		@n_expenses = params[:count].to_i
		@n_expenses.times{ @dummy_property_work.expenses.build(date: Date.today) }
	end

	def create
		params[:property_work][:expenses_attributes].each do |k, v|
			es_to_en_str!(v[:amount])
		end

		@property_work = PropertyWork.new property_work_params
		@property_work.finished = false

		if not @property_work.save
			signal_form_errors
			render 'new'
		else
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'trabajo en propiedad', description: "N ADM #{@property_work.property.administration_number.id}"
		end
	end

	def update

		@property_work = PropertyWork.find(params[:id])

		if not @property_work.update property_work_params
			puts @property_work.errors.inspect
		else
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'trabajo en propiedad', description: "N ADM #{@property_work.property.administration_number.id}"
		end
	end

	private

		def property_work_params
			params
				.require(:property_work)
				.permit(
					:property_id, :description, :provider,
					:start_date, :end_date, :finished,

					expenses_attributes:  Expense.attribute_names.map(&:to_sym).push(:_destroy).push(:voucher))
		end
end
