class SearchController < ApplicationController

	MODELS = {
		:user => User,
		:property => Property,
		:administration_number => AdministrationNumber,
		:contract => Contract,
		:settlement => Settlement,
		:expense => Expense,
		:payment => Payment,
		:log_entry => LogEntry
	}

	def search
		# Extract model
		model = MODELS[params[:model].to_sym]

		if params[:init_queries].present?
			params[:init_queries].values.each{|query| model = model.send(query.to_sym)}
		end

		# Extract constants
		constants = {}
		if params[:constants].present?
			params[:constants].each{ |k, v| constants[k.to_sym] = v }
		end

		# Put constants in query
		@query_result = model.where(constants)

		# Time queries
		if params[:time_vars].present?
			time_vars = JSON.parse params[:time_vars]
			if (not params[:from].nil?) and (not params[:until].nil?) and
			   (params[:from].present? or params[:until].present?)

				time_vars.each do |time_var|
					tokens = time_var.split '.'
					if tokens.length > 1
						joint_var = tokens[-1]
						last_model = tokens[-2]
						outter_models = tokens[0...-2]

						# JOINS
						join_hash = outter_models.reverse.inject(last_model.to_sym) do |v, k|
							{k.to_sym => [v]}
						end
						@query_result = @query_result.joins(join_hash)

						time_var = joint_var
					else
						last_model = model_name(params[:model])
					end

					if params[:from].present?
						@query_result = @query_result.where("#{last_model.pluralize}.#{time_var} >= ?", params[:from])
					elsif params[:until].present?
						@query_result = @query_result.where("#{last_model.pluralize}.#{time_var} <= ?", params[:until])
					end
				end
			end
		end

		# Parse search vars
		search_vars =  JSON.parse params[:search_vars]

		# Build query
		or_query = ''
		final_res = []
		search_vars.each do |var|

			# If it's a nested model build propper INNER JOINs
			tokens = var.split '.'
			if tokens.length > 1
				joint_var = tokens[-1]
				last_model = tokens[-2]
				outter_models = tokens[0...-2]

				# JOINS
				join_hash = outter_models.reverse.inject(last_model.to_sym) do |v, k|
					{k.to_sym => [v]}
				end
				col_result = @query_result.joins(join_hash)

				# Prep values for query
				last_model = model_name(last_model)
				var = joint_var

			# If it's only a column prep values for query
			else
				last_model = model_name(params[:model])
				col_result = @query_result
			end

			# Perform query and store results
			condition = "CAST(#{last_model.pluralize}.#{var} AS text) ILIKE ?"
			col_result.where(condition, "%#{params[:search_term]}%").where('').each{ |row|
				final_res << row
			}
		end

		# Process query values
		@query_result = final_res.uniq

		# Meta parameters
		@table_container_id = params[:table_container_id]
		@table_partial = params[:table_partial]
		@locals = {params[:query_name].to_sym => @query_result}
	end

	private

	def model_name(model)
		if ['proprietary', 'tenant'].include? model
			model = 'user'
		end

		return model
	end

	def accumulate_hashes(h1, h2)
		acc = h1

		if h2.hash?
			h2.each do |k, v|
				if acc[k].nil?
					acc[k] = v

				elsif v.hash?
					acc[k] = v

				else
					acc[k] += v
				end
			end
		else
			acc = h2
		end

		return acc
	end

	def accumulate(list, el)

		# el is Hash
		if el.is_a?(Hash)
			el_key = el.keys[0]
			el_value = el.values[0]

			idx = list.map{|e| e.is_a?(Hash) ? e.keys[0] : nil}.index{|e| e == el_key}
			if idx.nil?
				list << el
			else
				hash = list[idx]
				hash_key = hash.keys[0]
				hash_value = hash.values[0]
				list[idx][hash_key] = accumulate(hash_value, el_value).flatten
			end

		# el is Symbol or Array
		else
			if not list.include? el
				list << el
			end
		end

		return list
	end

	def accumulate_hash(h1, list)

	end

end
