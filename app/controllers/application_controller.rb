

class ApplicationController < ActionController::Base
    helper_method :current_user
    before_action :require_login

    add_template_helper ApplicationHelper

	# Handler neccesary for CanCan
	def current_user
		if session[:user_id]
			@current_user ||= User.find(session[:user_id])
		else
			@current_user = nil
		end
	end

	# Permission denial handler
	rescue_from CanCan::AccessDenied do |exception|
		flash[:forbidden] = "Usted no puede realizar esa acción"
		redirect_back fallback_location: '/'
	end


	def authorize_read(model)
		authorize! :read, model
	end

	def authorize_manage(model)
		authorize! :manage, model
	end

	def es_to_en_str!(str)
		str.gsub!('.', '')
		str.gsub!(',', '.')
	end

	def signal_form_errors
		flash[:alert] = "Hay errores en el formulario"
		flash[:alert] = "Hay errores en el formulario"
	end

	CHARS = ('0'..'9').to_a + ('A'..'Z').to_a + ('a'..'z').to_a
	def random_password(length=10)
	  CHARS.sort_by { rand }.join[0...length]
	end

	private

		def require_login
			unless current_user
				flash[:alert] = "Necesita estar logeado"
				redirect_to login_path
			end
		end
end
