class ExpensesController < ApplicationController
	before_action only:   [:index, :fetch_expenses, :fetch_expense] do authorize_read(Expense) end
	before_action except: [:index, :fetch_expenses, :fetch_expense] do authorize_manage(Expense) end

	def index
		@properties = Property.where(state: :rented)
	end

	def new
		@expense = Expense.new
		@property = Property.find(params[:property_id])
	end

	def edit
		@expense = Expense.find(params[:id])
		@property_has_expense = PropertyHasExpense.find_by(expense: @expense)
		@property = @property_has_expense.property

		render action: 'new'
	end

	def create
		es_to_en_str!(params[:expense][:amount])

		@property = Property.find(params[:expense][:property_id])
		@expense = Expense.new(expense_params)
		@property_has_expense = PropertyHasExpense.new(property: @property, expense: @expense)

		if @expense.save and @property_has_expense.save
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'gasto', description: "N ADM #{@expense.administration_number.id}"
			params[:property_id] = @property.id
			fetch_expenses
			render 'fetch_expenses'
		else
			render 'new'
		end
	end

	def update
		es_to_en_str!(params[:expense][:amount])

		@expense = Expense.find(params[:id])
		property_has_expense = PropertyHasExpense.find_by(expense: @expense)

		if @expense.update(expense_params)
		LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'gasto', description: "N ADM #{@expense.administration_number.id}"
			params[:property_id] = property_has_expense.property.id
			fetch_expenses
			render 'fetch_expenses'
		else
			@property = Property.find(@expense.property.id)
			render 'new'
		end
	end

	def destroy
		@expense = Expense.find(params[:id])
		admin_n = @expense.administration_number
		@expense.destroy

		LogEntry.create user: current_user, datetime: DateTime.now, action: 'eliminó', model: 'gasto', description: "N ADM #{admin_n.id}"

		params[:property_id] = @expense.property.id
		fetch_expenses
		render action: 'fetch_expenses'
	end

	# AJAX APIs
	# Fetches expenses for a given property
	def fetch_expenses
		@property = Property.find(params[:property_id])
		@expenses = PropertyHasExpense.where(property_id: params[:property_id]).collect{ |phe| phe.expense }
		expenses_from_property_works = PropertyWork.where(property_id: params[:property_id]).reduce([]){|acc, pw| acc = acc + pw.expenses}

		@expenses = @expenses + expenses_from_property_works
	end

	# Fetches expense given its id
	def fetch_expense
		@expense = Expense.find(params[:id])
	end

	private

		def expense_params
			params
				.require(:expense)
				.permit(
					:date, :amount, :description, :provider_name,
					:voucher_type, :voucher_number, :voucher,
					:proprietary, :contributions
				)
		end
end
