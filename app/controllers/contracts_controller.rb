class ContractsController < ApplicationController
	before_action only:   [:index, :edit] do authorize_read(Contract) end
	before_action except: [:index, :edit] do authorize_manage(Contract) end

	def index
		@contracts = Contract.all
	end

	def show
		@contract = Contract.find(params[:id])
		@last_inventory = Inventory.where(contract: @contract).order(:date).last
	end

	def new
		#@contract = Contract.new(rent: 0, warranty: 0, start_date: DateTime.now, expiration_date: DateTime.tomorrow)
		@contract = Contract.new(rent: 0, warranty: 0)
		@contract.amount_of_readjustments = 1
		@contract.readjustments.build(amount: 0)
	end

	def edit
		@editing = true
		@contract = Contract.find(params[:id])
		puts @contract.inspect
		render 'new'
	end

	def create
		es_to_en_str!(params[:contract][:daily_penalty_percentage])
		es_to_en_str!(params[:contract][:rent])
		es_to_en_str!(params[:contract][:warranty])
		params[:contract][:readjustments_attributes].each do |k, v|
			es_to_en_str!(v[:amount])
		end

		@contract = Contract.new(contract_params)
		@contract.active = true
		errors = false

		# Check if there is an admin number assigned
		if params[:administration_number] != ""
			@admin_n = AdministrationNumber.find(params[:administration_number])
			@admin_n.contract = @contract
		else
			@contract.valid?
			@contract.errors.add(:admin_n, 'Debe asignar un numero de administracion')
			puts @contract.errors.inspect
			return render 'new'
		end
		@admin_n.property.state = :rented

		# Check if an inventory was uploaded
		if params[:inventory]
			@contract.inventories.build(
				date: params[:inventory_date],
				file: params[:inventory]
			)
		end

		# Check if a discharge was uploaded
		if params[:discharge]
			@contract.discharges.build(
				date: params[:discharge_date],
				file: params[:discharge]
			)
		end

		if (not errors) and @contract.save and @admin_n.save and @admin_n.property.save
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'contrato', description: "N ADM #{@contract.administration_number.id}"
			redirect_to contracts_path
		else
			puts @contract.errors.inspect
			puts @admin_n.errors.inspect
			render 'new'
		end
	end

	def update
		es_to_en_str!(params[:contract][:daily_penalty_percentage])
		es_to_en_str!(params[:contract][:rent])
		es_to_en_str!(params[:contract][:warranty])
		params[:contract][:readjustments_attributes].each do |k, v|
			es_to_en_str!(v[:amount])
		end

		@contract = Contract.find(params[:id])

		# Check if an inventory was uploaded
		if params[:inventory]
			#inventory = Inventory.where(contract: @contract).last
			#inventory.update(date: params[:inventory_date], file: params[:inventory])
			@contract.inventories.build(
				date: params[:inventory_date],
				file: params[:inventory]
			)
		end

		# Check if a discharge was uploaded
		if params[:discharge]
			@contract.discharges.build(
				date: params[:discharge_date],
				file: params[:discharge]
			)
		end

		if @contract.update(contract_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'edito', description: "N ADM #{@contract.administration_number.id}"
			redirect_to contracts_path
		else
			puts @contract.errors.inspect
			render 'new'
		end

	end

	def terminate
		@contract = Contract.find(params[:contract][:id])
		@contract.active = false
		@contract.signal_termination

		# Release contract and tenant
		property = @contract.administration_number.property
		#@contract.administration_number.update(tenant: nil, contract: nil, property: nil, proprietary: nil)
		admin_n = @contract.administration_number.id
		@contract.administration_number.update(tenant: nil, contract: nil)
		@contract.update(administration_number: nil)
		property.update(state: :available)
		#property.update(administration_number: nil)

		if @contract.update(contract_termination_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'terminó', model: 'contrato', description: "N ADM #{admin_n}"
			redirect_to contracts_path
		else
			render 'show_terminate_form'
		end

	end

	# AJAX APIs
	def assign_admin_n
		@admin_n = AdministrationNumber.find(params[:admin_n])
	end

	def update_readjustment_fields
		@contract = Contract.new
		n_readjustments = params[:contract][:amount_of_readjustments].to_i
		n_readjustments.times {@contract.readjustments.build(amount: 0)}
	end

	def show_terminate_form
		@contract = Contract.find(params[:id])
	end

	def add_contract_expense
		@contract = Contract.find(params[:id])
		@count = params[:count].to_i
		@count.times{ @contract.contract_expenses.build(amount: 0) }
	end

	def show_discharge
		@discharge = ActiveStorage::Attachment.find(params[:discharge_id])
	end

	def show_inventory
		@inventory = ActiveStorage::Attachment.find(params[:inventory_id])
	end

	private

		def contract_params
			params
				.require(:contract)
				.permit(
					:expiration_notice,
					:start_date, :expiration_date, :daily_penalty_percentage,
					:days_for_penalty, :rent, :currency, :readjustment_term,
					:readjustment_type, :warranty, :warranty_payment,
					:amount_of_fees, :amount_of_readjustments,

					readjustments_attributes:  Readjustment.attribute_names.map(&:to_sym).push(:_destroy)
				)
		end

		def contract_termination_params
			params
				.require(:contract)
				.permit(:id, :termination_date, :termination_comment,

						contract_expenses_attributes: ContractExpense.attribute_names.map(&:to_sym).push(:_destroy))
		end
end
