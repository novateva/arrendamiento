class PropertiesController < ApplicationController
	before_action only:   [:index, :edit, :show_mandate] do authorize_read(Property) end
	before_action except: [:index, :edit, :show_mandate] do authorize_manage(Property) end

	def index
		@properties = Property.all
	end

	def new
		@users = User.proprietary
	end

	def new_property
		@user = User.find(params[:user_id])
		@property = Property.new
		@property.common_expenses_administrator = CommonExpensesAdministrator.new
		@property.administration_beneficiaries.build
		@admin_n = nil
	end

	def edit
		@property = Property.find(params[:id])
		puts @property.inspect
		if not @property.common_expenses_administrator
			@property.build_common_expenses_administrator
		end
		@user = @property.administration_number.proprietary
	end

	def create
		es_to_en_str!(params[:property][:water_ias])
		es_to_en_str!(params[:property][:electricity_ias])
		es_to_en_str!(params[:property][:gas_ias])
		es_to_en_str!(params[:property][:adm_percentage])
		params[:property][:administration_beneficiaries_attributes].each do |k, v|
			es_to_en_str!(v[:percentage])
		end

		@user = User.find(params[:user_id])
		@property = Property.new(property_params)
		@property.state = :available

		# Lookup admin_n if it was present
		if params[:administration_number] != ""
			@admin_n = AdministrationNumber.find(params[:administration_number])
		else
			@admin_n = nil
		end

		# Check if an admin_n was present and if it has no property associated
		if @admin_n != nil and @admin_n.property == nil
			@admin_n.property = @property
			@admin_n.proprietary = @user
		elsif @admin_n == nil
			@property.valid?
			@property.errors.add(:administration_number_select, 'Debe asignar un número de administración')
		elsif @admin_n.property != nil
			@property.valid?
			@property.errors.add(:administration_number_select, "La administración #{@admin_n.id} ya no está disponible")
		end

		if @property.errors.empty? and @property.save and @admin_n.save
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'propiedad', description: "N ADM #{@property.administration_number.id}"
			redirect_to properties_path
		else
			signal_form_errors
			if @property.common_expenses_administrator == nil
				@property.common_expenses_administrator = CommonExpensesAdministrator.new
			end
			render 'new_property'
		end
	end

	def update
		es_to_en_str!(params[:property][:water_ias])
		es_to_en_str!(params[:property][:electricity_ias])
		es_to_en_str!(params[:property][:gas_ias])
		es_to_en_str!(params[:property][:property_role_contribution])
		es_to_en_str!(params[:property][:parking_role_contribution])
		es_to_en_str!(params[:property][:cellar_role_contribution])
		es_to_en_str!(params[:property][:adm_percentage])
		params[:property][:administration_beneficiaries_attributes].each do |k, v|
			es_to_en_str!(v[:percentage])
		end

		@property = Property.find(params[:id])

		if @property.update(property_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'propiedad', description: "N ADM #{@property.administration_number.id}"
			@properties = Property.all
			render 'fetch_properties'
		else
			puts @property.errors.inspect
			@user = @property.administration_number.proprietary
			render 'edit'
		end
	end

	def delete
		@property = Property.find(params[:id])
	end

	def destroy
		@property = Property.find(params[:id])

		if @property.state.to_sym == :available
			@property.administration_number.release_property
			admin_n = @property.administration_number.id
			@property.destroy
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'eliminó', model: 'propiedad', description: "N ADM #{admin_n}"
		end

		@properties = Property.all
		render 'fetch_properties'
	end

	# AJAX APIs
	def add_administration_beneficiary
		@user = User.find(params[:user_id])
		@property = Property.new
		@count = params[:count].to_i
		@count.times{ @property.administration_beneficiaries.build }
	end

	def create_administration_number
		admin_n = AdministrationNumber.create
		LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'administración', description: "N ADM #{admin_n.id}"
		params[:admin_n] = admin_n.id
		assign_administration_number
		render 'assign_administration_number'
	end

	def assign_administration_number
		if not ['', '-1'].include? params[:admin_n]
			@admin_n = AdministrationNumber.find(params[:admin_n])
		else
			@admin_n = AdministrationNumber.new
		end

	end

	def show_mandate
		@property = Property.find(params[:id])
	end

	private

		def property_params
			params.require(:property)
				.permit(
					:property_type, :region, :commune, :common_expenses,
					:property_role_contribution, :parking_role_contribution,
					:cellar_role_contribution, :water_ias, :electricity_ias,
					:gas_ias, :address, :state, :adm_percentage, :adm_rent,
					:adm_contributions, :adm_common_expenses, :adm_other,
					:observations, :mandate,

					common_expenses_administrator_attributes: CommonExpensesAdministrator.attribute_names.map(&:to_sym).push(:_destroy),

					administration_beneficiaries_attributes: AdministrationBeneficiary.attribute_names.map(&:to_sym).push(:_destroy))
		end

end
