class UsersController < ApplicationController
	before_action only: [:new_proprietary, :new_tenant, :edit,
						 :create_proprietary, :create_tenant, :update,
						 :delete, :destroy] do authorize_manage(User) end

	def proprietaries
		@users = User.proprietary
		@role_name = 'Propietario'
		@role = :proprietary

		render 'index'
	end

	def new_proprietary
		@user = User.new(role: :proprietary)
		@user.build_representative
		@role_name = 'Propietario'

		render 'new_user'
	end

	def tenants
		@users = User.tenant
		@role_name = 'Arrendatario'
		@role = :tenant

		render 'index'
	end

	def new_tenant
		@user = User.new(role: :tenant)
		@user.build_representative
		@user.build_cosigner
		@administration_numbers = AdministrationNumber.without_tenant
		@role_name = 'Arrendatario'
		if params[:administration_number]
			@administration_number = params[:administration_number].to_i
		end

		render 'new_user'
	end

	def edit
		@user = User.find(params[:id])
	end

	def create_proprietary
		@user = User.new(user_params)
		password = random_password()
		@user.password = password

		if @user.save
			NotificationMailer.send_password(@user, password).deliver
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'usuario', description: "#{@user.name}"
			redirect_to @user.index_path

		else
			if not @user.representative.present?
				@user.build_representative
			end
			render 'new_user'
		end
	end

	def create_tenant
		@user = User.new(user_params)
		password = random_password()
		@user.password = password

		unless params[:administration_number]
			@user.valid?
			@user.errors.add(:administration_number, 'Debe seleccionar una propiedad')
			@administration_numbers = AdministrationNumber.without_tenant
			puts @user.errors.inspect
			signal_form_errors
			@user.build_representative
			@user.build_cosigner

			return render 'new_user'
		end

		@administration_number = AdministrationNumber.find(params[:administration_number])
		@administration_number.tenant = @user

		if @user.save and @administration_number.save
			NotificationMailer.send_password(@user, password).deliver
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'usuario', description: "#{@user.name}"
			redirect_to @user.index_path
		else
			signal_form_errors
			@administration_numbers = AdministrationNumber.without_tenant
			@user.build_representative
			@user.build_cosigner

			render 'new_user'
		end
	end

	def update
		@user = User.find(params[:id])

		if @user.update(user_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'usuario', description: "#{@user.name}"
			@users = User.send(@user.role.to_sym)
			@role_name = @user.role_string
			render 'fetch_users'

		else
			render 'edit'
		end
	end

	def delete
		@user = User.find(params[:id])
	end

	def destroy
		@user = User.find(params[:id])

		if @user.can_be_deleted?
			user_name = @user.name
			@user.administration_numbers.each do |n|
				n.update(@user.role => nil)
			end

			@user.destroy
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'eliminó', model: 'usuario', description: "#{user_name}"
		end

		@users = User.send(@user.role.to_sym)
		@role_name = @user.role_string
		render 'fetch_users'
	end

	# AJAX APIs
	def fetch_properties
		@user = User.find(params[:user_id])
		@admin_numbers = AdministrationNumber.where(proprietary_id: params[:user_id])
	end

	def fetch_settlements
		admin_numbers = AdministrationNumber.where(proprietary_id: params[:user_id])
		contracts = Contract.where(administration_number: admin_numbers)
		@user_settlements = Settlement.where(contract: contracts)

		admin_numbers = AdministrationNumber.where(proprietary_id: params[:user_id]).order(:id)
		@settlements_by_admin_number = Hash.new
		admin_numbers.each do |n|
			contract = Contract.where(administration_number: n)
			@settlements_by_admin_number[n.id] = Settlement.where(contract: contract)
		end

		@admin_numbers = AdministrationNumber.active_contract.where(proprietary_id: params[:user_id])
	end

	def fetch_tenant_payments
		@payments = Payment.where(user_id: params[:user_id])
		@user_id = params[:user_id]
	end

	def show_tenant_contract
		@contract = Contract.find(params[:contract_id])
	end

	def show_tenant_terminate_contract_form
		@contract = Contract.find(params[:contract_id])
	end

	private

		def user_params
			params.require(:user)
				.permit(
					:email, :password, :name, :email, :email2,
					:rut, :telephone, :telephone2, :address, :region,
					:commune, :observation, :role,

					representative_attributes: Representative.attribute_names.map(&:to_sym),
					cosigner_attributes:	   Cosigner.attribute_names.map(&:to_sym))
		end
end
