class SystemControlsController < ApplicationController
	before_action do authorize_manage(:system_controlls) end

	def info
		@company_info = Company.info
	end

	def update_info
		@company_info = Company.info

		@company_info.update(company_info_params)

		render 'info'
	end

	def users
		@user = User.new
		@internal_users = User.where(role: [:admin, :secretary, :executive, :office_chief]).order(:name)
		@external_users = User.where(role: [:tenant, :proprietary]).order(:name)
	end

	def create_user
		@user = User.new(user_params)

		# Validate only these parameters
		validations.each do |param, validator|
			if @user.send(param).send(validator)
				@user.errors.add(param, 'No puede estar vacío')
			end
		end

		if @user.errors.empty? and @user.save(validate: false)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'usuario', description: "#{@user.name}"
			redirect_to system_users_path
		else
			@internal_users = User.where(role: [:admin, :secretary, :executive, :office_chief]).order(:name)
			@external_users = User.where(role: [:tenant, :proprietary]).order(:name)
			render 'users'
		end
	end

	def edit_user
		@user = User.find(params[:user_id])
	end

	def update_user
		@user = User.find(params[:user_id])

		if @user.internal? and @user.update(user_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'usuario', description: "#{@user.name}"
			@users = User.where(role: [:admin, :secretary, :executive, :office_chief]).order(:name)
			render 'update_user'
		elsif not @user.internal? and @user.update(full_user_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'usuario', description: "#{@user.name}"
			@users = User.where(role: [:tenant, :proprietary]).order(:name)
			render 'update_user'
		else
			render 'edit_user'
		end
	end

	def activate_user
		@user = User.find(params[:user_id])

		@user.update(active: true)
		LogEntry.create user: current_user, datetime: DateTime.now, action: 'activó', model: 'usuario', description: "#{@user.name}"

		if @user.internal?
			@users = User.where(role: [:admin, :secretary, :executive, :office_chief]).order(:name)
		else @user.internal?
			@users = User.where(role: [:tenant, :proprietary]).order(:name)
		end


		render 'update_user'
	end

	def deactivate_user
		@user = User.find(params[:user_id])

		@user.update(active: false)
		LogEntry.create user: current_user, datetime: DateTime.now, action: 'desactivó', model: 'usuario', description: "#{@user.name}"

		if @user.internal?
			@users = User.where(role: [:admin, :secretary, :executive, :office_chief]).order(:name)
		else not @user.internal?
			@users = User.where(role: [:tenant, :proprietary]).order(:name)
		end

		render 'update_user'
	end

	def logs
		@logs = LogEntry.all.order(:datetime)
	end

	private

	def validations
		{:name => :empty?,
		 :rut => :empty?,
		 :role => :nil?,
		 :password => :nil?,
		 :email => :empty?
		}
	end

	def user_params
		params.require(:user).permit(:name, :rut, :role, :password, :email)
	end

	def full_user_params
		params.require(:user)
			.permit(
				:email, :password, :name, :email, :email2,
				:rut, :telephone, :telephone2, :address, :region,
				:commune, :observation, :role,

				representative_attributes: Representative.attribute_names.map(&:to_sym),
				cosigner_attributes:	   User.attribute_names.map(&:to_sym))
	end

	def company_info_params
		params.require(:company)
			.permit(
				:name, :rut, :address, :region, :commune, :email,
				:telephone1, :telephone2, :cellphone, :bank,
				:account_type, :titular, :account_number, :account_email
			)
	end
end
