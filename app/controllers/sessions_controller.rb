class SessionsController < ApplicationController
	skip_before_action :require_login, only: [:new, :create, :forgot_password, :restore_password]

	CHARS = ('0'..'9').to_a + ('A'..'Z').to_a + ('a'..'z').to_a
	def random_password(length=10)
	  CHARS.sort_by { rand }.join[0...length]
	end

	def new
	end

	def create
		user = User.find_by_rut(params[:rut])
		puts User.all.inspect
		if user && user.authenticate(params[:password])
			session[:user_id] = user.id
			redirect_to "/", notice: "Sesion iniciada"
		else
			flash[:alert] = "Datos invalidos"
			redirect_to login_path
		end
	end

	def destroy
		session[:user_id] = nil
		redirect_to login_path, notice: "Sesion cerrada"
	end

	def restore_password
		users = User.all
		user_emails = users.pluck(:email)
		success = false

		if user_emails.include? params[:email]
			user = User.where(email: params[:email]).last
			new_password = random_password(8)
			user.password = new_password

			if user.save
				NotificationMailer.restore_password(new_password, params[:email]).deliver
				@success = true
			end
		end
	end
end
