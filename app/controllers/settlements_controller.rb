class SettlementsController < ApplicationController
	before_action only:   [:index, :fetch_settlement, :fetch_settlements] do authorize_read(Settlement) end
	before_action except: [:index, :fetch_settlement, :fetch_settlements] do authorize_manage(Settlement) end

	def index
		@admin_numbers = AdministrationNumber.active_contract
	end

	def new
		# Query last settlement generated
		last_settlement = Settlement
			.where(contract_id: params[:contract_id])
			.order(:year)
			.order(:month)
			.last

		# Calculate next month and year
		if last_settlement.nil?
			contract_start_date = Contract.find(params[:contract_id]).start_date
			next_settlement_year = contract_start_date.year
			next_settlement_month = contract_start_date.month
			@settlement_number = 1
		else
			next_settlement_year = last_settlement.year
			next_settlement_month = last_settlement.month_number + 1
			@settlement_number = Settlement.where(contract_id: params[:contract_id]).count + 1
		end
		if next_settlement_month > 12
			next_settlement_month = 1
			next_settlement_year += 1
		end
		@settlement = Settlement.new(contract_id: params[:contract_id], year: next_settlement_year, month: next_settlement_month, date: Date.today, number: @settlement_number)
		@expenses = get_expenses(@settlement.contract.administration_number.property.id, next_settlement_year, next_settlement_month)
		calculate_totals

	end

	#def edit
	#	@settlement = Settlement.find(params[:id])
	#	@expenses = get_expenses(@settlement.contract.property.id, @settlement.year, Settlement.months[@settlement.month])
	#	@remaining_months_collection = get_remaining_month_collection(@settlement.contract_id, @settlement.year)
	#	calculate_totals
    #
	#	render 'new'
	#end

	def create
		# Create new settlement with provided params
		@settlement = Settlement.new(settlement_params)
		puts @settlement.inspect

		# Save and render settlements or form errors
		@settlement.generate_receipt_pdf
		@settlement.contract.generate_proprietary_payment_pdf(nil, @settlement)
		if @settlement.save
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'liquidación', description: "N ADM #{@settlement.contract.administration_number.id}"
			@settlements = Settlement.where(contract_id: params[:settlement][:contract_id])
			render 'fetch_settlements'
		else
			@expenses = get_expenses(@settlement.contract.administration_number.property.id, @settlement.year, @settlement.month_number)
			calculate_totals
			render 'new'
		end
	end

	#def update
	#	# Fetch current settlement
	#	@settlement = Settlement.find(params[:id])
    #
	#	# Update and render settlements or form errors
	#	if @settlement.update(settlement_params)
	#		@settlements = Settlement.where(contract_id: params[:settlement][:contract_id])
	#		render 'fetch_settlements'
	#	else
	#		@remaining_months_collection = get_remaining_month_collection(@settlement.contract_id, @settlement.year)
	#		@expenses = get_expenses(@settlement.contract.property.id, @settlement.year, Settlement.months[@settlement.month])
	#		calculate_totals
	#		render 'new'
	#	end
	#end

	# AJAX APIs
	# Fetches settlements made for a property
	def fetch_settlements
		@settlements = Settlement.where(contract_id: params[:contract_id])
		puts @settlements.inspect
	end

	# Updates months that dont yet have a settlement for a given year
	#def update_form_months
	#	@settlement = Settlement.new(contract_id: params[:contract_id], year: params[:settlement][:year])
	#	@remaining_months_collection = get_remaining_month_collection(params[:contract_id], params[:settlement][:year])
	#	current_month = Settlement.months[@remaining_months_collection.values[0]]
	#	@expenses = get_expenses(@settlement.contract.property.id, params[:settlement][:year], current_month)
	#	calculate_totals
    #
	#	render 'update_form_months'
	#end

	# Updates the expenses in the form
	#def update_form_expenses
	#	@settlement = Settlement.new(contract_id: params[:contract_id])
	#	month = Settlement.months[params[:settlement][:month]]
	#	@expenses = get_expenses(@settlement.contract.property.id, params[:year], month)
	#	calculate_totals
    #
	#	render 'update_form_expenses'
	#end

	# Searches a particular settlement and shows invoice
	def fetch_settlement
		@settlement = Settlement.find(params[:id])
	end

	def settlement_pdf
		@settlement = Settlement.find(params[:id])
		#@settlement.generate_receipt_pdf
		#@settlement.save
	end


	private

		def settlement_params
			params
				.require(:settlement)
				.permit(
					:contract_id, :month, :year, :transference, :invoice,
					:invoice_number, :date, :number
				)
		end

		#def get_remaining_month_collection(contract_id, year)
		#	current_settlements = Settlement.where(contract_id: contract_id, year: year)
		#	settled_months = current_settlements.pluck(:month).map{|m| m.to_sym}
		#	remaining_months = Settlement::MONTH_TO_STR.except(*settled_months)
		#	remaining_months.invert
		#end

		def get_expenses(property_id, year, month)
			expenses = []

			expenses += Expense
				.joins(:property_has_expense)
				.where(expenses: {proprietary: true})
				.where(property_has_expenses: {property_id: property_id})
				.where('extract(year from date) = ?', year)
				.where('extract(month from date) = ?', month)

			expenses += Expense
				.joins(property_work_has_expense: :property_work)
				.where(expenses: {proprietary: true})
				.where(property_works: {property_id: property_id})
				.where('extract(year from date) = ?', year)
				.where('extract(month from date) = ?', month)

			return expenses
		end

		def calculate_totals
			@total_discount = 0
			@expenses.each{ |e| @total_discount += e.amount }
			@amount_to_transfer = @settlement.contract.rent - @total_discount
		end
end
