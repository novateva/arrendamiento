class HomeController < ApplicationController

	def index
		today = Date.today

		@payments_to_charge = 0
		@payments_to_pay = 0
		@settlements_to_pay = 0
		@properties_with_works = 0
		AdministrationNumber.active_contract.each do |n|
			if n.delayed_months > 0
				@payments_to_charge += 1
			end

			#if n.delayed_settlements > 0
			#	@settlements_to_pay += 1
			#end
			@settlements_to_pay += n.delayed_settlements

			#if n.property and not n.property.property_works.empty?
			if n.property and not n.property.active_property_works.empty?
				@properties_with_works += 1
			end
		end

		@payments_to_pay = Payment.with_unpaid_settlement.length
		puts Payment.with_unpaid_settlement.inspect
	end
end
