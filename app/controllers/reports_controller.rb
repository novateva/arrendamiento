class ReportsController < ApplicationController
	before_action only: [:property_return, :active_administrations] do authorize_read(:executive_reports) end
	before_action except: [:property_return, :active_administrations] do authorize_read(:reports) end

	def collection
		@admin_numbers = AdministrationNumber.active_contract
	end

	def collection_debts
		@admin_number = AdministrationNumber.find(params[:admin_number])
		@debts = Array.new
		last_payment_year, last_payment_month = @admin_number.last_payment_date

		date_tmp = Date.new(last_payment_year, last_payment_month, @admin_number.contract.start_date.day)
		date_limit = Date.new(Date.today.year, Date.today.month, date_tmp.day)
		while date_tmp < date_limit
			@debts.push( {
				expiration_date: Date.new(date_tmp.year, date_tmp.month, @admin_number.contract.start_date.day),
				rent: @admin_number.contract.readjusted_rent(date_tmp.year, date_tmp.month),
				penalties: @admin_number.contract.penalty(date_tmp),
			})

			date_tmp += 1.month
		end

		respond_to do |format|
			format.js do
				render 'collection_debts.js.erb'
			end

			format.pdf do
				render(
					pdf: "DeudasPorCobrar#{@admin_number.id}",
					template: 'reports/collection_debts_pdf.html.erb',
					page_size: 'A4',
					layout: "receipt.html",
					orientation: "Landscape",
					lowquaility: true,
					zoom: 1,
					dpi: 75
				)
			end
		end
	end

	def generate_collection_debts_pdf

	end

	def record_matches_search(date, admin_number, proprietary_name, property_address)
		if params[:from].present? and date < params[:from].to_date
			false
		elsif params[:until].present? and date > params[:until].to_date
			false
		elsif params[:search_term].present? and not(admin_number.to_s.include? params[:search_term].downcase or
												    proprietary_name.downcase.include? params[:search_term].downcase or
												    property_address.downcase.include? params[:search_term].downcase)
			false
		else
			true
		end
	end

	def settlements
		@settlements = []
		AdministrationNumber.active_contract.each do |n|
			last_settlement_year, last_settlement_month = n.last_settlement_date
			tmp_settlement_date = Date.new(last_settlement_year, last_settlement_month, n.contract.start_date.day)
			tmp_settlement_date += 1.month
			current_date = Date.today

			while tmp_settlement_date <= current_date

				if record_matches_search(tmp_settlement_date, n.id, n.proprietary.name, n.property.address)
					@settlements.append({
						:admin_number => n.id,
						:proprietary_name => n.proprietary.name,
						:property_address => n.property.address,
						:date => tmp_settlement_date.strftime('%d-%m-%Y'),
						:delay_days => (current_date - tmp_settlement_date).to_i
					})
				end

				tmp_settlement_date += 1.month
			end
		end
	end

	def property_return
		@admin_numbers = AdministrationNumber.active_contract
	end

	def active_administrations
		@admin_numbers = AdministrationNumber.active_contract
	end

	def contributions
		@contributions = Expense.where(contributions: true)
	end

	def expenses
		@expenses = Expense.all
		@expenses.each do |e|
			puts e.property_has_expense.inspect
		end
	end

	def administration_profit
		@payments = Payment.all
	end

	def property_works
		@property_works = PropertyWork.all
	end

	def unpaid_settlements
		@unpaid_settlements = Payment.with_unpaid_settlement
	end

end
