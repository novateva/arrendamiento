class PaymentsController < ApplicationController
	before_action only:   [:index, :generate_receipt, :generate_guarantee_receipt] do authorize_read(Payment) end
	before_action except: [:index, :generate_receipt, :generate_guarantee_receipt] do authorize_manage(Payment) end

	def index
		@payments = Payment.all
		@admin_numbers = AdministrationNumber.active_contract
	end

	def show
		@payment = Payment.find(params[:id])
		@users = User.all
		@contracts = Contract.all
	end

	def new
		@payment = Payment.new(amount: 0, common_expense_amount: 0, month: Date.today.month)
		@admin_number = AdministrationNumber.find(params[:administration_number])

		# Lookup this year's payments for this contract
		paid_months = Payment.where(
			year: Date.today.year,
			contract: @admin_number.contract)
			.map {|payment| payment.month}

		contract_start_date = @admin_number.contract.start_date
		# Filter the months that have already been paid
		@unpaid_months_collection = Payment.month_collection
			.select{ |key, value| (Date.today.year != contract_start_date.year or Payment.months[value] >= contract_start_date.month) and not paid_months.include? value.to_s}

		@payment_number = Payment.where(contract: @admin_number.contract).count + 1

		@readjustment_by_month = {}
		months = Payment.months.keys
		start_date = Date.new(Date.today.year, 1, @admin_number.contract.start_date.day)
		for i in 0...12
			@readjustment_by_month[months[i]] = @admin_number.contract.rent_readjustment(start_date + i.months)
		end

		@delay_days_by_month = {}
		for i in 0...12
			tmp_date = Date.new(Date.today.year, i+1, @admin_number.contract.start_date.day)
			delay_days = (Date.today - tmp_date).to_i - @admin_number.contract.days_for_penalty
			delay_days = delay_days < 0 ? 0 : delay_days
			@delay_days_by_month[months[i]] = delay_days
		end
	end

	def new_warranty
		@admin_number = AdministrationNumber.find(params[:administration_number])
	end

	def edit
		@payment = Payment.find(params[:id])
		@users = User.all
		@contracts = Contract.all
	end

	def create
		es_to_en_str!(params[:difference])
		es_to_en_str!(params[:payment][:amount])
		es_to_en_str!(params[:payment][:common_expense_amount])

		@payment = Payment.new(payment_params)
		@payment.year = Date.today.year
		payment_date = Date.new(@payment.year, @payment.month_number, @payment.contract.start_date.day)
		payment_date = payment_date + (params[:delay_days]).to_i.days
		@payment.date = payment_date

		if params[:difference].to_i < 0
			@payment.errors.add(:amount, :not_enough, message: 'La diferencia no puede ser negativa')
			@admin_number = Contract.find(params[:payment][:contract_id]).administration_number
			paid_months = Payment.where(
				year: Date.today.year,
				contract: @admin_number.contract)
				.map {|payment| payment.month}

			contract_start_date = @admin_number.contract.start_date
			# Filter the months that have already been paid
			@unpaid_months_collection = Payment.month_collection
				.select{ |key, value| (Date.today.year != contract_start_date.year or Payment.months[value] >= contract_start_date.month) and not paid_months.include? value.to_s}
			@payment_number = Payment.where(contract: @admin_number.contract).count + 1

			@readjustment_by_month = {}
			months = Payment.months.keys
			start_date = Date.new(Date.today.year, 1, @admin_number.contract.start_date.day)
			for i in 0...12
				@readjustment_by_month[months[i]] = @admin_number.contract.rent_readjustment(start_date + i.months)
			end

			@delay_days_by_month = {}
			for i in 0...12
				tmp_date = Date.new(Date.today.year, i+1, @admin_number.contract.start_date.day)
				delay_days = (Date.today - tmp_date).to_i - @admin_number.contract.days_for_penalty
				delay_days = delay_days < 0 ? 0 : delay_days
				@delay_days_by_month[months[i]] = delay_days
			end

			return render 'new'
		end

		tenant = @payment.contract.administration_number.tenant
		prior_balance = tenant.balance
		tenant.balance = params[:difference].to_f

		@payment.generate_receipt_pdf(prior_balance)
		@payment.contract.generate_proprietary_payment_pdf(@payment, nil, prior_balance)
		if @payment.save and tenant.save
			NotificationMailer.payment(@payment).deliver
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'creó', model: 'pago', description: "N ADM #{@payment.contract.administration_number.id}"
			render 'create'
		else
			@admin_number = Contract.find(params[:payment][:contract_id]).administration_number
			# Lookup this year's payments for this contract
			paid_months = Payment.where(
				year: Date.today.year,
				contract: @admin_number.contract)
				.map {|payment| payment.month}

			contract_start_date = @admin_number.contract.start_date
			# Filter the months that have already been paid
			@unpaid_months_collection = Payment.month_collection
				.select{ |key, value| (Date.today.year != contract_start_date.year or Payment.months[value] >= contract_start_date.month) and not paid_months.include? value.to_s}
			@payment_number = Payment.where(contract: @admin_number.contract).count + 1

			@readjustment_by_month = {}
			months = Payment.months.keys
			start_date = Date.new(Date.today.year, 1, @admin_number.contract.start_date.day)
			for i in 0...12
				@readjustment_by_month[months[i]] = @admin_number.contract.rent_readjustment(start_date + i.months)
			end

			@delay_days_by_month = {}
			for i in 0...12
				tmp_date = Date.new(Date.today.year, i+1, @admin_number.contract.start_date.day)
				delay_days = (Date.today - tmp_date).to_i - @admin_number.contract.days_for_penalty
				delay_days = delay_days < 0 ? 0 : delay_days
				@delay_days_by_month[months[i]] = delay_days
			end

			render 'new'
		end
	end

	def update
		es_to_en_str!(params[:difference])
		es_to_en_str!(params[:payment][:amount])
		es_to_en_str!(params[:payment][:common_expense_amount])

		@payment = Payment.find(params[:id])

		if @payment.update(payment_params)
			LogEntry.create user: current_user, datetime: DateTime.now, action: 'editó', model: 'pago', description: "N ADM #{@payment.contract.administration_number.id}"
			redirect_to @payment
		else
			render 'edit'
		end
	end

	def destroy
		@payment = Payment.find(params[:id])
		@payment.destroy

		LogEntry.create user: current_user, datetime: DateTime.now, action: 'eliminó', model: 'pago', description: "N ADM #{@payment.contract.administration_number.id}"

		redirect_to payments_path
	end

	def generate_payment_receipt
		@payment = Payment.find(params[:payment_id])
	end

	def generate_warranty_receipt
		@admin_number = AdministrationNumber.find(params[:administration_number])
		@admin_number.contract.generate_warranty_receipt_pdf
		@admin_number.contract.paid_waranty_fees += 1
		@admin_number.contract.save
	end

	def show_proprietary_payment
		@admin_number = AdministrationNumber.find(params[:administration_number])
	end

	def update_discounts
		year = Date.today.year
		month = Payment.months[params[:month]]

		@admin_number = AdministrationNumber.find(params[:administration_number])

		@discounts = 0

		@discounts += Expense
			.joins(:property_has_expense)
			.where(expenses: {proprietary: false})
			.where(property_has_expenses: {property_id: @admin_number.property.id})
			.where('extract(year from date) = ?', year)
			.where('extract(month from date) = ?', month)
			.pluck(:amount)
			.sum

		@discounts += Expense
			.joins(property_work_has_expense: :property_work)
			.where(expenses: {proprietary: false})
			.where(property_works: {property_id: @admin_number.property.id})
			.where('extract(year from date) = ?', year)
			.where('extract(month from date) = ?', month)
			.pluck(:amount)
			.sum
	end

	def update_ipc
		uri = URI("https://api.sbif.cl/api-sbifv3/recursos_api/ipc/#{params[:year]}/#{params[:month]}?apikey=f39f3e325687640ecdcb805f113c0f9ef730b74f&formato=JSON")
		response = Net::HTTP.get_response(uri)
		@response_code = response.code
		@ipc = 0
		if response.code == '200'
			json = JSON.parse response.body
			@ipc = json['IPCs'][0]['Valor'].to_f
		end
	end

	private

		def payment_params
			params.require(:payment).permit(
				:user_id, :contract_id, :month, :year, :date, :amount,
				:payment_method, :common_expense_amount
			)
		end
end
