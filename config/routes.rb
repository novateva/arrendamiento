Rails.application.routes.draw do

	# Settlemets
	get 'settlements/fetch_settlements/:contract_id', to: 'settlements#fetch_settlements', as: 'fetch_settlements'
	get 'settlement/settlement_pdf/:id', to: 'settlements#settlement_pdf', as: 'settlement_pdf'
	get 'settlements/new/:contract_id', to: 'settlements#new', as: 'new_settlement'
	resources :settlements, except: [:new, :show, :destroy]

	# Payments
	resources :payments, except: [:new, :show]
	get 'payments/show_proprietary_payment/:administration_number', to: 'payments#show_proprietary_payment', as: 'show_proprietary_payment'
	get 'payments/:administration_number/generate_warranty_receipt', to: 'payments#generate_warranty_receipt', as: 'generate_warranty_receipt'
	get 'payments/new/:administration_number', to: 'payments#new', as: 'new_payment'
	get 'payments/new/update_discounts/:administration_number/:month', to: 'payments#update_discounts', as: 'payments_update_discounts'
	get 'payments/new/update_ipc/:year/:month', to: 'payments#update_ipc', as: 'payments_update_ipc'
	get 'payments/new/:payment_id/generate_receipt', to: 'payments#generate_payment_receipt', as: 'generate_payment_receipt'
	get 'payments/new/warranty/:administration_number', to: 'payments#new_warranty', as: 'new_warranty_payment'

	# Contracts
	post  'contracts/assign_admin_n', to: 'contracts#assign_admin_n', as: 'assign_admin_n'
	get   'contracts/update_readjustment_fields', to: 'contracts#update_readjustment_fields'
	get   'contracts/:id/show_terminate_form', to: 'contracts#show_terminate_form', as: 'show_termiante_form'
	get   'contracts/:id/show_terminate_form/add_expense/:count', to: 'contracts#add_contract_expense', as: 'add_contract_expense'
	get   'contracts/:id/show_warranty_receipt', to: 'contracts#show_warranty_receipt', as: 'contracts_show_warranty_receipt'
	patch 'contracts/:id/terminate', to: 'contracts#terminate', as: 'terminate_contract'
	get   'contracts/:id/show', to: 'contracts#show', as: 'show_contract'
	get   'contracts/show/discharge/:discharge_id', to: 'contracts#show_discharge', as: 'show_contract_discharge'
	get   'contracts/show/inventory/:inventory_id', to: 'contracts#show_inventory', as: 'show_contract_inventory'
	resources :contracts, except: [:show]

	# Expenses
	get 'expenses/fetch/:property_id', to: 'expenses#fetch_expenses', as: 'fetch_expenses'
	get 'expenses/new/:property_id', to: 'expenses#new', as: 'new_expense'
	get 'expenses/fetch_expense/:id', to: 'expenses#fetch_expense'
	resources :expenses, except: [:new, :show]

	# Associated Documents
	resources :associated_documents

	# Notifications
	resources :notifications

	# Properties
	resources :properties, except: [:show]
	get 'properties/new/:user_id', to: 'properties#new_property'
	get 'properties/new/:user_id/add_administration_beneficiary/:count', to: 'properties#add_administration_beneficiary', as: 'add_administration_beneficiary'
	get 'properties/create_administration_number', to: 'properties#create_administration_number', as: 'create_administration_number'
	get 'properties/assign_administration_number/:admin_n', to: 'properties#assign_administration_number', as: 'assign_administration_number'
	get 'properties/delete/:id', to: 'properties#delete', as: 'delete_property'
	get 'properties/show_mandate/:id', to: 'properties#show_mandate', as: 'show_property_mandate'

	# USERS
	get  'users/delete/:id', to: 'users#delete', as: 'delete_user'
	# Proprietaries
	get  'users/proprietaries', to: 'users#proprietaries'
	get  'users/new/proprietary', to: 'users#new_proprietary'
	get  'users/fetch_properties/:user_id', to: 'users#fetch_properties'
	get  'users/fetch_settlements/:user_id', to: 'users#fetch_settlements'
	post 'users/proprietaries', to: 'users#create_proprietary', as: 'create_proprietary'
	# Tenants
	get  'users/tenants', to: 'users#tenants'
	get  'users/new/tenant', to: 'users#new_tenant'
	get  'users/new/tenant/:administration_number', to: 'users#new_tenant', as: 'create_tenant_with_admin_n'
	post 'users/tenants', to: 'users#create_tenant', as: 'create_tenant'
	get  'users/tenant/:user_id/fetch/payments', to: 'users#fetch_tenant_payments', as: 'fetch_tenant_payments'
	get  'users/tenant/show/payment/receipt/:payment_id', to: 'users#show_tenant_payment_receipt', as: 'show_tenant_payment_receipt'
	get  'users/tenant/show/contract/:contract_id', to: 'users#show_tenant_contract', as: 'show_tenant_contract'
	get  'users/tenant/terminate_contract/:contract_id', to: 'users#show_tenant_terminate_contract_form', as: 'show_tenant_terminate_contract_form'
	resources :users, except: [:index, :show, :new]

	# Administration Numbers
	get 'administration/index', to: 'administration_numbers#index', as: 'administration_numbers'

	# Reports
	get 'reports/collection', to: 'reports#collection', as: 'reports_collection'
	get 'reports/collection_debts/:admin_number', to: 'reports#collection_debts', as: 'reports_collection_debts'
	get 'reports/collection_debts/:admin_number/generate.pdf', to: 'reports#collection_debts', as: 'reports_collection_debts_pdf'
	get 'reports/settlements', to: 'reports#settlements', as: 'reports_settlements'
	get 'reports/property_return', to: 'reports#property_return', as: 'reports_property_return'
	get 'reports/active_administrations', to: 'reports#active_administrations', as: 'reports_active_administrations'
	get 'reports/contributions', to: 'reports#contributions', as: 'reports_contributions'
	get 'reports/expenses', to: 'reports#expenses', as: 'reports_expenses'
	get 'reports/administration_profit', to: 'reports#administration_profit', as: 'reports_administration_profit'
	get 'reports/property_works', to: 'reports#property_works', as: 'reports_property_works'
	get 'reports/unpaid_settlements', to: 'reports#unpaid_settlements', as: 'reports_unpaid_settlements'

	# Property Works
	get   'property_works', to: 'property_works#index', as: 'property_works'
	get   'property_works/show/:property_id', to: 'property_works#show', as: 'show_property_works'
	get   'property_works/new/:property_id', to: 'property_works#new', as: 'new_property_work'
	get   'property_works/new/add_expense/:count', to: 'property_works#add_expense', as: 'property_work_add_expense'
	post  'property_works', to: 'property_works#create'
	patch 'property_works/:id', to: 'property_works#update', as: 'update_property_work'

	# System Control
	get   'system_controls/info', to: 'system_controls#info', as: 'system_info'
	patch 'system_controls/update_info', to: 'system_controls#update_info', as: 'system_update_info'
	get   'system_controls/users', to: 'system_controls#users', as: 'system_users'
	post  'system_controls/users', to: 'system_controls#create_user', as: 'system_create_user'
	get   'system_controls/users/edit/:user_id', to: 'system_controls#edit_user', as: 'system_edit_user'
	patch 'system_controls/users/:user_id', to: 'system_controls#update_user', as: 'system_update_user'
	get   'system_controls/users/deactivate/:user_id', to: 'system_controls#deactivate_user', as: 'system_deactivate_user'
	get   'system_controls/users/activate/:user_id', to: 'system_controls#activate_user', as: 'system_activate_user'
	get   'system_controls/logs', to: 'system_controls#logs', as: 'system_logs'

	# Search
	post 'search', to: 'search#search', as: 'search'

	# Sessions
	resources :sessions, only: [:create, :destroy]
	get  'signup', to: 'users#new', as: 'signup'
	get  'login',  to: 'sessions#new', as: 'login'
	get  'logout', to: 'sessions#destroy', as: 'logout'
	get  'forgot_password', to: 'sessions#forgot_password', as: 'forgot_password'
	post 'restore_password', to: 'sessions#restore_password', as: 'restore_password'


	# Home and login
	get 'home/index', as: 'home'

	# Root definition
	root 'home#index'
end
