# CONFIGURAR DB

Para configurar el usuario en Postgres y en el sistema:
    
        $ sudo -u postgres createuser -d novateva_admin
        $ sudo -u postgres psql
        # \password novateva_admin
        <ingresar contraseña>
        # \q
        $ sudo adduser novateva_admin
        <ingresar datos>
    
Para ingresar a la DB con el cliente de terminal:

        $ psql -U novateva_admin -h 127.0.0.1 -d <nombre_database>
        
Para inicializar la DB:

        $ cd <directorio_aplicacion>
        $ ./bin/rails db:create
        $ ./bin/rails db:setup
        
# PAQUETES NECESARIOS

Wicked pdf: https://wkhtmltopdf.org/downloads.html

# ESTRUCTURA DE ARCHIVOS

Deben asegurarse la siguiente estructura dentro de la carpeta public:

    public/
        uploads/
            receipts/
            transferences/
            invoices/
