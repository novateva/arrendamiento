# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# company = Company.create :razon_social => "razon socialtest", :nombre_fantasia => "nombre fantasia test", :rut => "rut test",:giro => "giro test", :pais => "pais test", :ciudad => "ciudad test", :direccion => "direccion tes", :tipo => :admin
puts 'Seeding...'
test_pdf_path = './public/test_pdf.pdf'
test_image_path = './public/test_image.png'

# Account info
Company.create!(
	:name           => "CIVITANIC",
	:rut            => "",
	:address        => "",
	:region         => 0,
	:commune        => 0,

	:email          => "",
	:telephone1     => "",
	:telephone2     => "",
	:cellphone      => "",

	:bank           => 0,
	:account_type   => 0,
	:titular        => "",
	:account_number => "",
	:account_email  => ""
)

# USERS
# Admin
admin_user = User.create!(
	:email	   => "admin@admin.com",
	:password  => "admin",
	:role	   => :admin,
	:name	   => 'Admin',
	:telephone => '123',
	:rut	   => '12.345.678-9',
	:address   => 'Calle La Arboleda',
	:region	   => 0,
	:commune   => 0
)

# Secretary
secretary_user = User.create!(
	:email	   => "secretary@secretary.com",
	:password  => "secretary",
	:role 	   => :secretary,
	:name 	   => 'Secretary',
	:telephone => '123',
	:rut 	   => '98.765.432-1',
	:address   => 'Calle Las Palmas',
	:region    => 0,
	:commune   => 0
)

# Executive
executive_user = User.create!(
	:email 	   => "executive@executive.com",
	:password  => "executive",
	:role 	   => :executive,
	:name	   => 'Executive',
	:telephone => '123',
	:rut 	   => '12.369.874-5',
	:address   => 'Calle Ejecución',
	:region    => 0,
	:commune   => 0
)

# Guest
guest_user = User.create!(
	:email		=>"guest@guest.com",
	:password	=> "guest",
	:role		=> :guest,
	:name		=> 'Guest',
	:telephone	=> '123',
	:rut		=> '36.987.412-5',
	:address	=> 'Calle algo',
	:region		=> 0,
	:commune	=> 0
)

=begin
# Proprietaries
representative2 = Representative.new(
	:name 		=> 'Juan',
	:rut		=> '12.365.478-9',
	:telephone	=> '123',
	:email		=> 'j.c@gmail.com'
)
proprietary_user = User.new(
	:email			=> "proprietary@proprietary.com",
	:password		=> "proprietary",
	:role			=> :proprietary,
	:name			=> 'Proprietary',
	:telephone		=> '123',
	:rut			=> '95.135.746-2',
	:address		=> 'Calle Las Acacias',
	:region			=> 0,
	:commune		=> 0,
	:representative => representative2
)
proprietary_user.save!

# Properties
common_expenses_administrator = CommonExpensesAdministrator.new(
	:name 			=> 'Julio',
	:rut 			=> '11.111.111-1',
	:address 		=> 'Calle Misa',
	:region 		=> 0,
	:commune 		=> 0,
	:telephone 		=> '123456',
	:email 			=> 'cea@gmail.com',
	:bank 			=> 0,
	:account_type	=> 0,
	:account_number => '123',
	:titular		=> 'Joseito',
)
admn1 = AdministrationNumber.create!

property1 = Property.new(
	:property_type 				=> 0,
	:region						=> 0,
	:commune					=> 0,
	:common_expenses 			=> true,
	:property_role_contribution => '100-200',
	:parking_role_contribution  => '100-200',
	:cellar_role_contribution 	=> '100-200',
	:water_ias 					=> 500,
	:electricity_ias 			=> 500,
	:gas_ias 					=> 500,
	:address 					=> "Calle los Alamos 234",
	:state 						=> 0,

	:common_expenses_administrator => common_expenses_administrator,

	:adm_percentage			 	=> 5.5,
	:adm_rent 					=> true,
	:adm_contributions 			=> true,
	:adm_common_expenses		=> true,
	:adm_other 					=> nil,
	:observations 				=> nil,

	:administration_number => admn1
)
property1.administration_beneficiaries.build(
	:bank => :banco_de_chile,
	:account_type => :checking,
	:account_number => '123456',
	:titular => 'Andres Escalona',
	:percentage => 100,
	:email => 'aescalona@gmail.com',
	:rut => '12.434.567-7'
)
property1.mandate.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
property1.save!
admn1.update! :proprietary => proprietary_user

# Tenants
representative1 = Representative.new(
	:name 	   => 'Juan',
	:rut	   => '22.222.222-2',
	:telephone => '123',
	:email	   => 'repr@email.com'
)
cosigner1 = Cosigner.new(
	:email	   => "cosigner@tenant.com",
	:name 	   => 'Tenant',
	:telephone => '123',
	:rut 	   => '33.333.333-3',
	:address   => 'Calle Esperanza',
	:region	   => 0,
	:commune   => 0
)
tenant_user = User.new(
	:email	   		=> "casillajec@gmail.com",
	:password  		=> "tenant",
	:role 	   		=> :tenant,
	:name	   		=> 'Juan Casilla',
	:telephone 		=> '123',
	:rut	   		=> '44.444.444-4',
	:address   		=> 'Calle Bonanza',
	:region    		=> 0,
	:commune   		=> 0,
	:representative => representative1,
	:cosigner		=> cosigner1
)
tenant_user.save!
admn1.update! :tenant => tenant_user
=end

=begin
tenant_user2 = User.new(
	:email			=> "tenant2@tenant.com",
	:password 		=> "tenant",
	:role			=> :tenant,
	:name			=> 'Tenanto',
	:telephone		=> '123',
	:rut			=> '123',
	:address		=> 'Calle Malpica',
	:region			=> 0,
	:commune		=> 0,
	:representative => representative1,
	:cosigner		=> cosigner1
)
tenant_user2.save!

# Administration Numbers
admn2 = AdministrationNumber.create!
admn3 = AdministrationNumber.create!
admn4 = AdministrationNumber.create!
admn5 = AdministrationNumber.create!



property2 = Property.new(
	:property_type 				=> 0,
	:region						=> 0,
	:commune					=> 0,
	:common_expenses 			=> false,
	:property_role_contribution => 10.0,
	:parking_role_contribution  => 10.0,
	:cellar_role_contribution 	=> 10.0,
	:water_ias 					=> 10.0,
	:electricity_ias 			=> 20.0,
	:gas_ias 					=> 30.0,
	:address 					=> "Calle Milan 566",
	:state 						=> 0,

	:adm_percentage			 	=> 5.5,
	:adm_rent 					=> true,
	:adm_contributions 			=> true,
	:adm_common_expenses		=> true,
	:adm_other 					=> nil,
	:observations 				=> nil,

	:administration_number => admn2
)
property2.mandate.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
property2.save!
admn2.update! :proprietary => proprietary_user, :tenant => tenant_user2

property3 = Property.new(
	:property_type 				=> 0,
	:region						=> 0,
	:commune					=> 0,
	:common_expenses 			=> false,
	:property_role_contribution => 10.0,
	:parking_role_contribution  => 10.0,
	:cellar_role_contribution 	=> 10.0,
	:water_ias 					=> 10.0,
	:electricity_ias 			=> 20.0,
	:gas_ias 					=> 30.0,
	:address 					=> "Calle Milan 566",
	:state 						=> :available,

	:adm_percentage			 	=> 5.5,
	:adm_rent 					=> true,
	:adm_contributions 			=> true,
	:adm_common_expenses		=> true,
	:adm_other 					=> nil,
	:observations 				=> nil,

	:administration_number => admn3
)
property3.mandate.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
property3.save!
admn3.update! :proprietary => proprietary_user


# Contracts
contract1 = Contract.new(
	:expiration_notice			=> :days30,
	:start_date					=> DateTime.now,
	:expiration_date			=> DateTime.now.next_month,
	:daily_penalty_percentage	=> 0.05,
	:days_for_penalty			=> 5,
	:rent						=> 500.0,
	:currency					=> :peso,
	:readjustment_term			=>:months3,
	:readjustment_type			=> :rtype_ipc,
	:warranty					=> 450.0,
	:warranty_payment			=> :total,
	:amount_of_fees				=> 6,
	:amount_of_readjustments	=> 2,
	:active						=> true
)
contract1.inventories.build(date: Date.today )
contract1.inventories.each{|i| i.file.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )}
contract1.readjustments.build(
	:until		=> contract1.start_date.next_week,
	:amount		=> 0.0,
	:currency	=> :peso
)
contract1.readjustments.build(
	:until		=> contract1.expiration_date,
	:amount		=> 50.0,
	:currency	=> :peso
)
contract1.save!
admn1.update! :contract => contract1
property1.update! :state => :rented

contract2 = Contract.new(
	:expiration_notice			=> :days30,
	:start_date					=> DateTime.now,
	:expiration_date			=> DateTime.now.next_month,
	:daily_penalty_percentage	=> 0.05,
	:days_for_penalty			=> 5,
	:rent						=> 500.0,
	:currency					=> :peso,
	:readjustment_term			=> :months3,
	:readjustment_type			=> :rtype_ipc,
	:warranty					=> 450.0,
	:warranty_payment			=> :total,
	:amount_of_fees				=> 6,
	:amount_of_readjustments	=> 1,
	:active						=> true
)
contract2.inventories.build(date: Date.today)
contract2.inventories.each{|i| i.file.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )}
contract2.readjustments.build :until => contract2.expiration_date, :amount => 50.0, :currency => :peso
contract2.save!
admn2.update! :contract => contract2
property2.update! :state => :rented

# Expenses
#expense1 = Expense.new :property_id => property1.id, :date => DateTime.now, :amount => 100.0, :description => "Bombona de gas", :provider_name => "Corpoelec", :voucher_type => 0, :voucher_number => 1, :proprietary => true
expense1 = Expense.new :date => DateTime.now, :amount => 100.0, :description => "Bombona de gas", :provider_name => "Corpoelec", :voucher_type => 0, :voucher_number => 1, :proprietary => true
expense1.voucher.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
expense1.save!
PropertyHasExpense.create! :property_id => property1.id, :expense_id => expense1.id
#expense2 = Expense.new :property_id => property1.id, :date => DateTime.now, :amount => 100.0, :description => "Tanque de agua", :provider_name => "Corpoelec", :voucher_type => 0, :voucher_number => 2, :proprietary => false
expense2 = Expense.new :date => DateTime.now, :amount => 100.0, :description => "Tanque de agua", :provider_name => "Corpoelec", :voucher_type => 0, :voucher_number => 2, :proprietary => false
expense2.voucher.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
expense2.save!
PropertyHasExpense.create! :property_id => property1.id, :expense_id => expense2.id
#expense3 = Expense.new :property_id => property1.id, :date => DateTime.now, :amount => 100.0, :description => "Reparaciones"  , :provider_name => "Corpoelec", :voucher_type => 0, :voucher_number => 3, :proprietary => true
expense3 = Expense.new :date => DateTime.now, :amount => 100.0, :description => "Reparaciones"  , :provider_name => "Corpoelec", :voucher_type => 0, :voucher_number => 3, :proprietary => true
expense3.voucher.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
expense3.save!
PropertyHasExpense.create! :property_id => property1.id, :expense_id => expense3.id

# Payments
payment1 = Payment.create!(
	:user_id => tenant_user.id,
	:contract_id => contract1.id,
	:date => DateTime.now,
	:year => 2019,
	:month => 5,
	:amount => 500.0,
	:payment_method => 0
)

payment2 = Payment.create!(
	:user_id => tenant_user.id,
	:contract_id => contract1.id,
	:date => DateTime.now.tomorrow,
	:year => 2019,
	:month => 6,
	:amount => 510.0,
	:payment_method => 0
)

payment3 = Payment.create!(
	:user_id => tenant_user.id,
	:contract_id => contract1.id,
	:date => DateTime.now.tomorrow.tomorrow,
	:year => 2019,
	:month => 7,
	:amount => 1000.0,
	:payment_method => 0
)

# Settlements

settlement1 = Settlement.new(
	:contract_id => contract1.id,
	:date => Date.today,
	:month => :september,
	:year => 2019,
	:invoice_number => 1
)
settlement1.invoice.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
settlement1.transference.attach(io: File.open(test_pdf_path), filename: 'test_pdf.pdf' )
settlement1.save!
=end
puts 'Finished seeding!'
