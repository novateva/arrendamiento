# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_26_014230) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "administration_beneficiaries", force: :cascade do |t|
    t.bigint "property_id", null: false
    t.integer "bank", null: false
    t.integer "account_type", null: false
    t.string "account_number", null: false
    t.string "titular", null: false
    t.float "percentage", null: false
    t.string "email", null: false
    t.string "rut", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_administration_beneficiaries_on_property_id"
  end

  create_table "administration_numbers", force: :cascade do |t|
    t.bigint "proprietary_id"
    t.bigint "tenant_id"
    t.bigint "property_id"
    t.bigint "contract_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id"], name: "index_administration_numbers_on_contract_id"
    t.index ["property_id"], name: "index_administration_numbers_on_property_id"
    t.index ["proprietary_id"], name: "index_administration_numbers_on_proprietary_id"
    t.index ["tenant_id"], name: "index_administration_numbers_on_tenant_id"
  end

  create_table "associated_documents", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_associated_documents_on_user_id"
  end

  create_table "common_expenses_administrators", force: :cascade do |t|
    t.bigint "property_id", null: false
    t.string "name", null: false
    t.string "rut", null: false
    t.string "address", null: false
    t.integer "region", null: false
    t.integer "commune", null: false
    t.string "telephone"
    t.string "telephone2"
    t.string "email"
    t.string "email2"
    t.integer "bank", null: false
    t.integer "account_type", null: false
    t.string "account_number", null: false
    t.string "titular", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_common_expenses_administrators_on_property_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", null: false
    t.string "rut", null: false
    t.string "address", null: false
    t.integer "region", null: false
    t.integer "commune", null: false
    t.string "email", null: false
    t.string "telephone1", null: false
    t.string "telephone2", null: false
    t.string "cellphone", null: false
    t.integer "bank", null: false
    t.integer "account_type", null: false
    t.string "titular", null: false
    t.string "account_number", null: false
    t.string "account_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contract_expenses", force: :cascade do |t|
    t.bigint "contract_id", null: false
    t.string "description", null: false
    t.float "amount", null: false
    t.date "date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id"], name: "index_contract_expenses_on_contract_id"
  end

  create_table "contracts", force: :cascade do |t|
    t.integer "expiration_notice", null: false
    t.date "start_date", null: false
    t.date "expiration_date", null: false
    t.float "daily_penalty_percentage", null: false
    t.integer "days_for_penalty", null: false
    t.float "rent", null: false
    t.integer "currency", null: false
    t.integer "readjustment_term", null: false
    t.integer "readjustment_type", null: false
    t.float "warranty", null: false
    t.integer "warranty_payment", null: false
    t.integer "amount_of_fees", null: false
    t.integer "amount_of_readjustments", null: false
    t.boolean "active", null: false
    t.date "termination_date"
    t.text "termination_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "paid_waranty_fees", default: 0, null: false
  end

  create_table "cosigners", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "email"
    t.string "email2"
    t.string "name", null: false
    t.string "rut", null: false
    t.string "telephone"
    t.string "telephone2"
    t.string "address"
    t.integer "region"
    t.integer "commune"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_cosigners_on_user_id"
  end

  create_table "discharges", force: :cascade do |t|
    t.bigint "contract_id", null: false
    t.date "date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id"], name: "index_discharges_on_contract_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.date "date", null: false
    t.float "amount", null: false
    t.text "description", null: false
    t.string "provider_name"
    t.integer "voucher_type", null: false
    t.integer "voucher_number"
    t.boolean "proprietary", null: false
    t.boolean "contributions", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.bigint "contract_id", null: false
    t.date "date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id"], name: "index_inventories_on_contract_id"
  end

  create_table "ipc_readjustments", force: :cascade do |t|
    t.integer "year", null: false
    t.integer "month", null: false
    t.float "value", null: false
  end

  create_table "log_entries", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.datetime "datetime", null: false
    t.string "action", null: false
    t.string "model", null: false
    t.string "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_log_entries_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.text "text", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "payments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "contract_id", null: false
    t.integer "month", null: false
    t.integer "year", null: false
    t.date "date", null: false
    t.float "amount", null: false
    t.float "common_expense_amount", null: false
    t.integer "payment_method", default: 3, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id"], name: "index_payments_on_contract_id"
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "properties", force: :cascade do |t|
    t.integer "property_type", null: false
    t.integer "region", null: false
    t.integer "commune", null: false
    t.boolean "common_expenses", null: false
    t.string "property_role_contribution", null: false
    t.string "parking_role_contribution", null: false
    t.string "cellar_role_contribution", null: false
    t.integer "water_ias", null: false
    t.integer "electricity_ias", null: false
    t.integer "gas_ias", null: false
    t.text "address", null: false
    t.integer "state", null: false
    t.float "adm_percentage", null: false
    t.boolean "adm_rent", null: false
    t.boolean "adm_contributions", null: false
    t.boolean "adm_common_expenses", null: false
    t.string "adm_other"
    t.text "observations"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_has_expenses", force: :cascade do |t|
    t.bigint "expense_id", null: false
    t.bigint "property_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["expense_id"], name: "index_property_has_expenses_on_expense_id"
    t.index ["property_id"], name: "index_property_has_expenses_on_property_id"
  end

  create_table "property_work_has_expenses", force: :cascade do |t|
    t.bigint "expense_id", null: false
    t.bigint "property_work_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["expense_id"], name: "index_property_work_has_expenses_on_expense_id"
    t.index ["property_work_id"], name: "index_property_work_has_expenses_on_property_work_id"
  end

  create_table "property_works", force: :cascade do |t|
    t.bigint "property_id", null: false
    t.string "description", null: false
    t.string "provider", null: false
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.boolean "finished", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_property_works_on_property_id"
  end

  create_table "readjustments", force: :cascade do |t|
    t.bigint "contract_id", null: false
    t.date "until", null: false
    t.float "amount", null: false
    t.integer "currency", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id", "until"], name: "index_readjustments_on_contract_id_and_until", unique: true
    t.index ["contract_id"], name: "index_readjustments_on_contract_id"
  end

  create_table "representatives", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name", null: false
    t.string "rut", null: false
    t.string "telephone", null: false
    t.string "email", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_representatives_on_user_id"
  end

  create_table "settlements", force: :cascade do |t|
    t.bigint "contract_id", null: false
    t.integer "month", null: false
    t.integer "year", null: false
    t.integer "invoice_number", null: false
    t.date "date", null: false
    t.integer "number", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_id", "month", "year"], name: "index_settlements_on_contract_id_and_month_and_year", unique: true
    t.index ["contract_id"], name: "index_settlements_on_contract_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "email2"
    t.string "password_digest", null: false
    t.string "name", null: false
    t.string "rut", null: false
    t.string "telephone"
    t.string "telephone2"
    t.string "address"
    t.integer "region"
    t.integer "commune"
    t.integer "role", default: 5, null: false
    t.text "observation"
    t.float "balance", default: 0.0
    t.boolean "active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "administration_beneficiaries", "properties"
  add_foreign_key "administration_numbers", "contracts"
  add_foreign_key "administration_numbers", "properties"
  add_foreign_key "administration_numbers", "users", column: "proprietary_id"
  add_foreign_key "administration_numbers", "users", column: "tenant_id"
  add_foreign_key "associated_documents", "users"
  add_foreign_key "common_expenses_administrators", "properties"
  add_foreign_key "contract_expenses", "contracts"
  add_foreign_key "cosigners", "users"
  add_foreign_key "discharges", "contracts"
  add_foreign_key "inventories", "contracts"
  add_foreign_key "notifications", "users"
  add_foreign_key "payments", "contracts"
  add_foreign_key "payments", "users"
  add_foreign_key "property_has_expenses", "expenses"
  add_foreign_key "property_has_expenses", "properties"
  add_foreign_key "property_work_has_expenses", "expenses"
  add_foreign_key "property_work_has_expenses", "property_works"
  add_foreign_key "property_works", "properties"
  add_foreign_key "readjustments", "contracts"
  add_foreign_key "representatives", "users"
  add_foreign_key "settlements", "contracts"
end
