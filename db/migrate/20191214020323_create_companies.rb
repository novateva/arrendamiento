class CreateCompanies < ActiveRecord::Migration[5.2]
	def change
		create_table :companies do |t|

			t.string  :name,           null: false
			t.string  :rut,            null: false
			t.string  :address,        null: false
			t.integer :region,         null: false
			t.integer :commune,        null: false

			t.string  :email,          null: false
			t.string  :telephone1,     null: false
			t.string  :telephone2,     null: false
			t.string  :cellphone,      null: false

			t.integer :bank,           null: false
			t.integer :account_type,   null: false
			t.string  :titular,        null: false
			t.string  :account_number, null: false
			t.string  :account_email,  nill: false

			t.timestamps
		end
	end
end
