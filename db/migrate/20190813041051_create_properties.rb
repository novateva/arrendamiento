class CreateProperties < ActiveRecord::Migration[5.2]
	def change
		create_table :properties do |t|
			# Basic information
			#t.references :user,		   			   	  null: false, foreign_key: true
			t.integer	 :property_type,			  null: false
			t.integer	 :region,					  null: false
			t.integer	 :commune,					  null: false
			t.boolean	 :common_expenses, 		      null: false
			t.string	 :property_role_contribution, null: false
			t.string	 :parking_role_contribution,  null: false
			t.string	 :cellar_role_contribution,   null: false
			t.integer	 :water_ias, 				  null: false
			t.integer	 :electricity_ias, 			  null: false
			t.integer	 :gas_ias, 				   	  null: false
			t.text		 :address, 				   	  null: false
			t.integer	 :state,					  null: false

			# Administration mandate
			t.float		 :adm_percentage,	  	  	  null: false
			t.boolean	 :adm_rent,					  null: false
			t.boolean	 :adm_contributions,		  null: false
			t.boolean	 :adm_common_expenses,		  null: false
			t.string	 :adm_other
			t.text		 :observations

			#t.references :administration_number,	  null: false, foreign_key: true

			t.timestamps
		end
	end
end

