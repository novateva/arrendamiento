class CreateUsers < ActiveRecord::Migration[5.2]
	def change
		create_table :users do |t|
			t.string  :email, 		    null: false
			t.string  :email2, 		    null: true
			t.string  :password_digest, null: false
			t.string  :name, 	 		null: false
			t.string  :rut,				null: false
			t.string  :telephone,		null: true
			t.string  :telephone2,		null: true
			t.string  :address,			null: true
			t.integer :region,			null: true
			t.integer :commune,			null: true
			t.integer :role,			null: false, default: 5
			t.text	  :observation,		null: true
			t.float	  :balance,			null: true, default: 0
			t.boolean :active,			null: false, default: true

			t.timestamps()
		end

		#add_index :users, :email, unique: true
	end
end
