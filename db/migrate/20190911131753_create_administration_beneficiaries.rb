class CreateAdministrationBeneficiaries < ActiveRecord::Migration[5.2]
	def change
		create_table :administration_beneficiaries do |t|
			t.references :property,		  null: false, foreign_key: true
			t.integer	 :bank, 		  null: false
			t.integer	 :account_type,	  null: false
			t.string	 :account_number, null: false
			t.string	 :titular, 	   	  null: false
			t.float		 :percentage,	  null: false
			t.string	 :email,		  null: false
			t.string	 :rut,			  null: false

			t.timestamps
		end
	end
end
