class CreateRepresentatives < ActiveRecord::Migration[5.2]
	def change
		create_table :representatives do |t|
			t.references :user,		 null: false, foreign_key: true
			t.string	 :name, 	 null: false
			t.string	 :rut,		 null: false
			t.string	 :telephone, null: false
			t.string	 :email, 	 null: false

			t.timestamps
		end
	end
end
