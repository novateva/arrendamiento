class CreatePropertyHasExpenses < ActiveRecord::Migration[5.2]
	def change
		create_table :property_has_expenses do |t|

			t.references :expense, null: false, foreign_key: true
			t.references :property,	null: false, foreign_key: true

			t.timestamps
		end
	end
end
