class CreateContracts < ActiveRecord::Migration[5.2]
	def change
		create_table :contracts do |t|
			t.integer	 :expiration_notice,		null: false
			t.date       :start_date,			   	null: false
			t.date       :expiration_date,		   	null: false
			t.float		 :daily_penalty_percentage,	null: false
			t.integer	 :days_for_penalty,			null: false
			t.float		 :rent,						null: false
			t.integer	 :currency,					null: false
			t.integer	 :readjustment_term,		null: false
			t.integer	 :readjustment_type,		null: false
			t.float		 :warranty,					null: false
			t.integer	 :warranty_payment,			null: false
			t.integer	 :amount_of_fees,			null: false
			t.integer	 :amount_of_readjustments,	null: false
			t.boolean	 :active,					null: false

			# Optional termination fields
			t.date		 :termination_date
			t.text		 :termination_comment

			t.timestamps
		end

		#add_index :contracts, [:property_id, :start_date, :expiration_date], unique: true, name: 'index_contracts_on_property_start_expiraton_dates'
	end
end
