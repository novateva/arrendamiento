class CreateSettlements < ActiveRecord::Migration[5.2]
	def change
		create_table :settlements do |t|
			t.references :contract, 			  null: false, foreign_key: true
			t.integer	 :month,				  null: false
			t.integer	 :year,					  null: false
			t.integer	 :invoice_number,		  null: false
			t.date		 :date,					  null: false
			t.integer	 :number,				  null: false

			t.timestamps
		end

		add_index :settlements, [:contract_id, :month, :year], unique: true
	end
end
