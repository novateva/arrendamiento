class CreatePropertyWorks < ActiveRecord::Migration[5.2]
	def change
		create_table :property_works do |t|
			t.references :property, null: false, foreign_key: true
			t.string	 :description, null: false
			t.string	 :provider, null: false
			t.date		 :start_date, null: false
			t.date		 :end_date, null: false
			t.boolean	 :finished, null: false

			t.timestamps
		end
	end
end
