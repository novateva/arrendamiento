class CreateIpcReadjustments < ActiveRecord::Migration[5.2]
	def change
		create_table :ipc_readjustments do |t|
			t.integer :year,  null: false
			t.integer :month, null: false
			t.float	  :value, null: false
		end
	end
end
