class CreatePropertyWorkHasExpenses < ActiveRecord::Migration[5.2]
	def change
		create_table :property_work_has_expenses do |t|
			t.references :expense, 		 null: false, foreign_key: true
			t.references :property_work, null: false, foreign_key: true

			t.timestamps
		end
	end
end
