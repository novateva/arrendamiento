class CreatePayments < ActiveRecord::Migration[5.2]
	def change
		create_table :payments do |t|
			t.references :user, 				 null: false, foreign_key: true
			t.references :contract,				 null: false, foreign_key: true
			t.integer	 :month,				 null: false
			t.integer	 :year,					 null: false
			t.date		 :date,					 null: false
			t.float		 :amount,				 null: false
			t.float		 :common_expense_amount, null: false
			t.integer	 :payment_method,		 null: false, default: 3
			t.timestamps
		end
	end
end
