class CreateCommonExpensesAdministrators < ActiveRecord::Migration[5.2]
	def change
		create_table :common_expenses_administrators do |t|
			t.references :property,		  null: false, foreign_key: true
			t.string	 :name,			  null: false
			t.string 	 :rut,			  null: false
			t.string 	 :address,		  null: false
			t.integer 	 :region,		  null: false
			t.integer 	 :commune,		  null: false
			t.string 	 :telephone
			t.string 	 :telephone2
			t.string 	 :email
			t.string 	 :email2
			t.integer	 :bank,			  null: false
			t.integer	 :account_type,	  null: false
			t.string 	 :account_number, null: false
			t.string 	 :titular,		  null: false

			t.timestamps
		end
	end
end
