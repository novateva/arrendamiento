class CreateAdministrationNumbers < ActiveRecord::Migration[5.2]
	def change
		create_table :administration_numbers do |t|
			t.references :proprietary, index: true, foreign_key: {to_table: :users}
			t.references :tenant, 	   index: true, foreign_key: {to_table: :users}
			t.references :property, 				foreign_key: true
			t.references :contract, 				foreign_key: true

			t.timestamps
		end
	end
end
