class CreateLogEntries < ActiveRecord::Migration[5.2]
	def change
		create_table :log_entries do |t|
			t.references :user,			null: false
			t.datetime   :datetime,		null: false
			t.string	 :action,		null: false
			t.string	 :model,		null: false
			t.string	 :description,	null: false

			t.timestamps
		end
	end
end
