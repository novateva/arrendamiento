class CreateCosigners < ActiveRecord::Migration[5.2]
	def change
		create_table :cosigners do |t|
			t.references :user,				null: false, foreign_key: true
			t.string  	 :email, 		    null: true
			t.string  	 :email2, 		    null: true
			t.string  	 :name, 	 		null: false
			t.string  	 :rut,				null: false
			t.string  	 :telephone,		null: true
			t.string  	 :telephone2,		null: true
			t.string  	 :address,			null: true
			t.integer 	 :region,			null: true
			t.integer 	 :commune,			null: true

			t.timestamps
		end
	end
end
