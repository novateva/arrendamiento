class CreateContractExpenses < ActiveRecord::Migration[5.2]
	def change
		create_table :contract_expenses do |t|

			t.references :contract, 	null: false, foreign_key: true
			t.string 	 :description, 	null: false
			t.float		 :amount, 		null: false
			t.date		 :date, 		null: false

			t.timestamps
		end
	end
end
