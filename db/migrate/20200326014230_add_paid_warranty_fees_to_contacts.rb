class AddPaidWarrantyFeesToContacts < ActiveRecord::Migration[5.2]
  def change
	add_column :contracts, :paid_waranty_fees, :int, null: false, default: 0
  end
end
