class CreateExpenses < ActiveRecord::Migration[5.2]
	def change
		create_table :expenses do |t|
			#t.references :property,			 null: false, foreign_key: true
			t.date		 :date,				 null: false
			t.float		 :amount,			 null: false
			t.text		 :description,		 null: false
			t.string	 :provider_name
			t.integer	 :voucher_type,		 null: false
			t.integer	 :voucher_number
			t.boolean	 :proprietary,		 null: false
			t.boolean	 :contributions,	 null: false
			t.timestamps
		end
	end
end
