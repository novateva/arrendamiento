class CreateReadjustments < ActiveRecord::Migration[5.2]
	def change
		create_table :readjustments do |t|
			t.references :contract, null: false, foreign_key: true

			t.date		 :until,	null: false
			t.float		 :amount,	null: false
			t.integer	 :currency,	null: false

			t.timestamps
		end

		add_index :readjustments, [:contract_id, :until], unique: true
	end
end
