# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/notification_publication
  def notification_publication
    UserMailer.notification_publication
  end

  def register
    UserMailer.register(User.first)
  end

end
