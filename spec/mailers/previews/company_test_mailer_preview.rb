# Preview all emails at http://localhost:3000/rails/mailers/company_test_mailer
class CompanyTestMailerPreview < ActionMailer::Preview

    def started_diagnostic_preview
        CompanyTestMailer.started_diagnostic(CompanyTest.where(test_type: :diagnostic, state: :en_progresso).first)
    end

    def completed_diagnostic_preview
        CompanyTestMailer.completed_diagnostic(User.first)
    end

    def observation_added_preview
        CompanyTestMailer.observation_added(User.first)
    end

    def assigned_improvement_preview
        CompanyTestMailer.assigned_improvement(CompanyTest.last)
    end

    def answered_observation_preview
        CompanyTestMailer.answered_observation(CompanyTest.last)
    end
end
