require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe "notification_publication" do
    let(:mail) { UserMailer.notification_publication }

    it "renders the headers" do
      expect(mail.subject).to eq("Notification publication")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
