require 'rails_helper'

RSpec.describe "answers/edit", type: :view do
  before(:each) do
    @answer = assign(:answer, Answer.create!(
      :text => "MyText",
      :points => 1,
      :question => nil,
      :index => 1
    ))
  end

  it "renders the edit answer form" do
    render

    assert_select "form[action=?][method=?]", answer_path(@answer), "post" do

      assert_select "textarea[name=?]", "answer[text]"

      assert_select "input[name=?]", "answer[points]"

      assert_select "input[name=?]", "answer[question_id]"

      assert_select "input[name=?]", "answer[index]"
    end
  end
end
