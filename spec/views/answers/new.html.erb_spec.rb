require 'rails_helper'

RSpec.describe "answers/new", type: :view do
  before(:each) do
    assign(:answer, Answer.new(
      :text => "MyText",
      :points => 1,
      :question => nil,
      :index => 1
    ))
  end

  it "renders new answer form" do
    render

    assert_select "form[action=?][method=?]", answers_path, "post" do

      assert_select "textarea[name=?]", "answer[text]"

      assert_select "input[name=?]", "answer[points]"

      assert_select "input[name=?]", "answer[question_id]"

      assert_select "input[name=?]", "answer[index]"
    end
  end
end
