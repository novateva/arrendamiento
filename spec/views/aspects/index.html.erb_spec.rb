require 'rails_helper'

RSpec.describe "aspects/index", type: :view do
  before(:each) do
    assign(:aspects, [
      Aspect.create!(
        :name => "Name"
      ),
      Aspect.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of aspects" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
