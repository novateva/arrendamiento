require 'rails_helper'

RSpec.describe "aspects/edit", type: :view do
  before(:each) do
    @aspect = assign(:aspect, Aspect.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit aspect form" do
    render

    assert_select "form[action=?][method=?]", aspect_path(@aspect), "post" do

      assert_select "input[name=?]", "aspect[name]"
    end
  end
end
