require 'rails_helper'

RSpec.describe "questions/new", type: :view do
  before(:each) do
    assign(:question, Question.new(
      :company_test => nil,
      :type => 1,
      :aspect => nil,
      :criteria => nil,
      :text => "MyText"
    ))
  end

  it "renders new question form" do
    render

    assert_select "form[action=?][method=?]", questions_path, "post" do

      assert_select "input[name=?]", "question[company_test_id]"

      assert_select "input[name=?]", "question[type]"

      assert_select "input[name=?]", "question[aspect_id]"

      assert_select "input[name=?]", "question[criteria_id]"

      assert_select "textarea[name=?]", "question[text]"
    end
  end
end
