require 'rails_helper'

RSpec.describe "company_tests/edit", type: :view do
  before(:each) do
    @company_test = assign(:company_test, CompanyTest.create!(
      :state => 1,
      :type => 1,
      :user => nil
    ))
  end

  it "renders the edit company_test form" do
    render

    assert_select "form[action=?][method=?]", company_test_path(@company_test), "post" do

      assert_select "input[name=?]", "company_test[state]"

      assert_select "input[name=?]", "company_test[type]"

      assert_select "input[name=?]", "company_test[user_id]"
    end
  end
end
