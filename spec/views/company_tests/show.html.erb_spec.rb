require 'rails_helper'

RSpec.describe "company_tests/show", type: :view do
  before(:each) do
    @company_test = assign(:company_test, CompanyTest.create!(
      :state => 2,
      :type => 3,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(//)
  end
end
