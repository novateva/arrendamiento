require 'rails_helper'

RSpec.describe "company_tests/index", type: :view do
  before(:each) do
    assign(:company_tests, [
      CompanyTest.create!(
        :state => 2,
        :type => 3,
        :user => nil
      ),
      CompanyTest.create!(
        :state => 2,
        :type => 3,
        :user => nil
      )
    ])
  end

  it "renders a list of company_tests" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
