require 'rails_helper'

RSpec.describe "company_tests/new", type: :view do
  before(:each) do
    assign(:company_test, CompanyTest.new(
      :state => 1,
      :type => 1,
      :user => nil
    ))
  end

  it "renders new company_test form" do
    render

    assert_select "form[action=?][method=?]", company_tests_path, "post" do

      assert_select "input[name=?]", "company_test[state]"

      assert_select "input[name=?]", "company_test[type]"

      assert_select "input[name=?]", "company_test[user_id]"
    end
  end
end
