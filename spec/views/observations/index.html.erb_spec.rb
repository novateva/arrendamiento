require 'rails_helper'

RSpec.describe "observations/index", type: :view do
  before(:each) do
    assign(:observations, [
      Observation.create!(
        :observation => "MyText",
        :state => 2,
        :path_evidence => "Path Evidence",
        :answer => nil
      ),
      Observation.create!(
        :observation => "MyText",
        :state => 2,
        :path_evidence => "Path Evidence",
        :answer => nil
      )
    ])
  end

  it "renders a list of observations" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Path Evidence".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
