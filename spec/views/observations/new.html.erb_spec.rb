require 'rails_helper'

RSpec.describe "observations/new", type: :view do
  before(:each) do
    assign(:observation, Observation.new(
      :observation => "MyText",
      :state => 1,
      :path_evidence => "MyString",
      :answer => nil
    ))
  end

  it "renders new observation form" do
    render

    assert_select "form[action=?][method=?]", observations_path, "post" do

      assert_select "textarea[name=?]", "observation[observation]"

      assert_select "input[name=?]", "observation[state]"

      assert_select "input[name=?]", "observation[path_evidence]"

      assert_select "input[name=?]", "observation[answer_id]"
    end
  end
end
