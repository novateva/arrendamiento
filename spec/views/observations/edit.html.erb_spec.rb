require 'rails_helper'

RSpec.describe "observations/edit", type: :view do
  before(:each) do
    @observation = assign(:observation, Observation.create!(
      :observation => "MyText",
      :state => 1,
      :path_evidence => "MyString",
      :answer => nil
    ))
  end

  it "renders the edit observation form" do
    render

    assert_select "form[action=?][method=?]", observation_path(@observation), "post" do

      assert_select "textarea[name=?]", "observation[observation]"

      assert_select "input[name=?]", "observation[state]"

      assert_select "input[name=?]", "observation[path_evidence]"

      assert_select "input[name=?]", "observation[answer_id]"
    end
  end
end
