require 'rails_helper'

def login_via_form(email, password)
    visit "/"
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    click_button "Iniciar Sesión"
end

def logout_via_form()
    find('.navbar-link').click
    click_button 'Cerrar Sesión'
end

describe "User", type: :feature, :js => true do
    
    it "Flow core" do 
        login_via_form('admin@admin.com', '123456')
        
        visit "/companies/new"

        fill_in 'company_razon_social', with: 'Razon social test'
        fill_in 'company_rut', with: '12.345.677-1'
        fill_in 'company_giro', with: 'giro test'
        fill_in 'company_pais', with: 'pais test'
        fill_in 'company_ciudad', with: 'ciudad test'
        fill_in 'company_direccion', with: 'direccion test'
        click_button "Crear empresa"

        click_link "Nuevo Usuario"
        
        fill_in 'user_nombre', with: 'Nombre 1 test'
        fill_in 'user_email', with: 'oscarsoler25@gmail.com'
        fill_in 'user_password', with: '123456'
        fill_in 'user_password_confirmation', with: '123456'
        click_button "Crear usuario"
        

        expect(page).to have_content('oscarsoler25@gmail.com')
        sleep(inspection_time=2)

        logout_via_form()
        
        #------------------Create publication ---------- # 

        login_via_form('oscarsoler25@gmail.com', '123456')
        #expect(page).to have_content('Bienvenido')

        visit "/publications/"
        click_link 'Nueva publicación' 
        
        fill_in 'publication_cargo', with: 'Test cargo'
        fill_in 'publication_cantidad', with: '1'
        fill_in 'publication_ciudad', with: 'Test ciudad'
        #fill_in 'publication_tipo_cargo', with: 'temporal'
        fill_in 'publication_especificaciones_academicas', with: 'Test esp academicas'
        fill_in 'publication_especificaciones_laborales', with: 'Test esp laborales'
        # fill_in 'publication_especificaciones_otras', with: 'Test esp otras'
        fill_in 'publication_fecha', with: '28/02/2019'
        click_button 'Crear publicación'

        expect(page).to have_selector("#publication_cargo[value='Test cargo']")
        
        click_button 'Enviar'

        #expect(page).to have_content('enviada')

        sleep(inspection_time=2)

        # ------------------ Create process ----------# 
        logout_via_form()
        login_via_form('admin@admin.com', '123456') 
        visit '/recruitments'
        sleep(inspection_time=2)
        click_link 'Crear Proceso'
        fill_in 'recruitment_observaciones', with:'observaciones generales test'
        
        fill_in 'recruitment_candidates_attributes_0_name', with:'canditado nombre 1'

        fill_in 'recruitment_candidates_attributes_0_observacion', with:'canditado 1'

        click_button 'Crear proceso'
        expect(page).to have_field('', type: 'textarea', with: 'observaciones generales test')
        click_button 'Enviar cliente'
        #expect(page).to have_content('terminado')

        # ------------------ Verify process externo ----------# 
        logout_via_form()
        login_via_form('oscarsoler25@gmail.com', '123456') 
        visit 'publications'
        click_link 'Ver proceso'
        expect(page).to have_content('observaciones generales test')
        expect(page).to have_content('canditado 1')
        sleep(inspection_time=4)
    end
end


