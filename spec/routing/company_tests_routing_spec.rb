require "rails_helper"

RSpec.describe CompanyTestsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/company_tests").to route_to("company_tests#index")
    end

    it "routes to #new" do
      expect(:get => "/company_tests/new").to route_to("company_tests#new")
    end

    it "routes to #show" do
      expect(:get => "/company_tests/1").to route_to("company_tests#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/company_tests/1/edit").to route_to("company_tests#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/company_tests").to route_to("company_tests#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/company_tests/1").to route_to("company_tests#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/company_tests/1").to route_to("company_tests#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/company_tests/1").to route_to("company_tests#destroy", :id => "1")
    end
  end
end
