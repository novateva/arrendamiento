namespace :notification do
	desc "Update user notifications"

	task update: :environment do

	end

	task rent_expiration: :environment do

		today = Date.today

		AdministrationNumber.active_contract.each do |n|
			max_payment_day = n.contract.payment_day + n.contract.days_for_penalty.days
			last_payment = Payment.where(user: n.tenant).order(:year).order(:month).last

			# If there is no payment for this month registered
			# and the rent is one week from expiring, send email
			if last_payment.year < max_payment_day.year and
			   last_payment.month < max_payment_day.month

				if max_payment_day - 7 < today and today <= max_payment_day
					NotificationMailer.rent_expiration(n).deliver
				elsif today > max_payment_day
					NotificationMailer.rent_expired(n).deliver
				end
			end

		end
	end

	task expiring_contracts: :environment do
		today = Date.today
		contracts = Contract.where(active: true).where('expiration_date <= ?', today + 7)

		NotificationMailer.expiring_contract(contracts).deliver
	end

	task rent_readjustment: :environment do
		today = Date.today

		AdministrationNumber.active_contract.each do |admin_number|
			readjustments = admin_number.contract.readjustments
			readjustments.each_with_index do |readjustment, i|
				if i != (readjustments.length() -1) and readjustment.until + 1.day == today
					NotificationMailer.rent_readjustment(admin_number, readjustments[i+1]).deliver
				end
			end
		end
	end

end
