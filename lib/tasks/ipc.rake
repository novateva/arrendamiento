namespace :ipc do
	desc "TODO"
	task update_ipc: :environment do

		this_year = Date.today.year

		missing_months = (1..12).to_a
		IpcReadjustment.for_year(this_year).each do |ipc|
			if ipc.value != -1
				missing_months.delete(ipc.month)
			end
		end

		missing_months.each do |month|
			begin
				uri = URI("https://api.sbif.cl/api-sbifv3/recursos_api/ipc/#{this_year}/#{month}?apikey=f39f3e325687640ecdcb805f113c0f9ef730b74f&formato=JSON")
				response = Net::HTTP.get_response(uri)
				response_code = response.code
				ipc = -1
				if response.code == '200'
					json = JSON.parse response.body
					ipc = json['IPCs'][0]['Valor'].to_f
				end
			rescue
				ipc = -1
			end

			IpcReadjustment.create(year: this_year, month: month, value: ipc)
		end

	end
end
