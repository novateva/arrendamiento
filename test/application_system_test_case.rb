require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
	driven_by :selenium, using: :chrome, screen_size: [1400, 1400]

	client = Selenium::WebDriver::Remote::Http::Default.new
	client.read_timeout = 120 # seconds
	driver = Selenium::WebDriver.for :chrome, http_client: client
end
