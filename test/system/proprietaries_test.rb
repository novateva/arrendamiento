require "application_system_test_case"

class ProprietariesTest < ApplicationSystemTestCase
	test "visit_index" do
		login 'admin', 'admin'
		visit '/users/proprietaries'
		assert_selector 'h4', text: 'Ver Propietario'
	end

	def login(username, password)
		visit '/login'
		fill_in 'Rut', with: username
		fill_in 'Contraseña', with: password
		click_on 'Ingresar'
		assert true
	end
end
