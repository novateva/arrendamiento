var excelTable = document.getElementById('excel-table');
var tbody = excelTable.getElementsByTagName('tbody')[0];
var unpaidSettlementsTable = document.getElementById('unpaid-settlements-table');
var settlementsTbody = unpaidSettlementsTable.getElementsByTagName('tbody')[0];
var rows = unpaidSettlementsTable.rows;

function addRowsToExcel(){
	var rowsToRemove = [];
	for(var i = 1; i < rows.length; i++){
		var row = rows[i];
		var selected = row.getElementsByClassName('excel-checkbox')[0].checked;

		if(selected){
			var excelTableFields = row.getElementsByClassName('excel-field');
			var tr = document.createElement('tr');

			while(excelTableFields.length > 0){
				var td = excelTableFields[0];
				td.hidden = false;
				tr.appendChild(td);
			}
			tbody.appendChild(tr);
			rowsToRemove.push(row);
		}
	}

	for(var i = 0; i < rowsToRemove.length; i++){
		settlementsTbody.removeChild(rowsToRemove[i]);
	}
}
;
